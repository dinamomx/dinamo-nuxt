const PORT = 3000

module.exports = {
  /**
   * Application configuration section
   * http://pm2.keymetrics.io/docs/usage/application-declaration/
   * Importante: Cambiar el puerto de acuerdo a la configuración de nginx
   */
  apps: [
    // First application
    {
      name: 'plantilla.dinamo.mx',
      script: 'node_modules/nuxt/bin/nuxt start',
      env: {
        HOST: 'localhost',
        NODE_ENV: 'production',
      },
      env_staging: {
        PORT,
        DEBUG: 'nuxt*,din*,app*',
      },
      env_production: {
        PORT,
        PRODUCTION: true,
        DEBUG: null,
      },
    },
  ],

  /**
   * Deployment section
   * http://pm2.keymetrics.io/docs/usage/deployment/
   */
  deploy: {
    production: {
      user: 'webdev',
      host: 'droplet.dinamo.mx',
      ref: 'origin/Tailwind',
      repo: 'git@bitbucket.org:dinamomx/dinamo-nuxt.git',
      path: '/var/www/plantilla.dinamo.mx',
      'pre-deploy-local':
        'PORT=3000 HOST=localhost NODE_ENV=production pnpm run build; sh uploadbuild.sh',
      'pre-deploy':
        'pnpm install --prefer-frozen-shrinkwrap --prod --reporter append-only',
      'post-deploy': 'pm2 startOrReload ecosystem.config.js --env production',
    },
    staging: {
      user: 'webdev',
      host: 'droplet.dinamo.mx',
      ref: 'origin/develop',
      repo: 'git@bitbucket.org:dinamomx/dinamo-nuxt.git',
      path: '/var/www/plantilla.dinamo.mx',
      'pre-deploy-local': 'sh scripts/build-upload.sh',
      'pre-deploy':
        'pnpm install --prod --prefer-frozen-shrinkwrap --reporter append-only',
      'post-deploy': 'pm2 startOrReload ecosystem.config.js --env staging',
    },
  },
}
