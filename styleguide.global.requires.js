import Vue from 'vue'
import buefy from 'buefy'
// eslint-disable-next-line import/no-extraneous-dependencies
import NuxtLink from '@nuxt/vue-app/template/components/nuxt-link.server'

Vue.use(buefy)
Vue.component('nuxt-link', NuxtLink)
