/* eslint-disable no-param-reassign */
/**
 * @type {import('@nuxt/types').Plugin}
 */
const vueGtag = ({ app, env }) => {
  if (process.env.NODE_ENV === 'production') {
    const headAttributes = {
      link: [
        {
          rel: 'preload',
          as: 'script',
          href: `https://www.googletagmanager.com/gtag/js?id=${env.GA_ID}`,
        },
        {
          rel: 'preconnect',
          href: 'https://www.google-analytics.com/',
        },
      ],
    }
    if (!app.head) {
      app.head = headAttributes
    } else if (typeof app.head !== 'function') {
      app.head.link = [...(app.head.link || []), ...headAttributes.link]
    } else {
      // eslint-disable-next-line no-console
      console.warn('Cant add preload links if head is a function')
    }
  }
}

export default vueGtag
