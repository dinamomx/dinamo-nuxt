import cookie from 'cookie'
import { COOKIE_NAME } from './tracking.client'
/**
 * Funcionalidad para el manejo de rastreo.
 * Depende de vue-gtag.
 *
 * @version 1.0
 * @type {import('@nuxt/types').Plugin}
 */
const trackingPluginServer = ({ req }, inject) => {
  let status = 'unknown'
  if (!process.static) {
    const cookies = cookie.parse(req.headers.cookie || '')
    status = cookies[COOKIE_NAME]
  }

  /** @type {import('types/vuetracking').TrackingPlugin} */
  const tracking = {
    status,
    explicitEnable: () => null,
    implictEnable: () => null,
    enable: () => null,
    initialEnable: () => null,
    disableTracking: () => null,
  }
  inject('tracking', tracking)
}

export default trackingPluginServer
