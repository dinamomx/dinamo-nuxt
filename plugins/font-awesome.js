import Vue from 'vue'

import { library, config } from '@fortawesome/fontawesome-svg-core'
import {
  faPlus,
  faEnvelope,
  faPhone,
  faBars,
  faExclamationCircle,
} from '@fortawesome/pro-light-svg-icons'
// import { faQuestionCircle } from '@fortawesome/pro-regular-svg-icons'

import { faDownload } from '@fortawesome/pro-solid-svg-icons'
import { faFacebook, faWhatsapp } from '@fortawesome/free-brands-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

config.autoAddCss = false
config.showMissingIcons = true

library.add(
  faPlus,
  faEnvelope,
  faFacebook,
  faWhatsapp,
  faPhone,
  faBars,
  faDownload,
  faExclamationCircle
)

Vue.component('FaIcon', FontAwesomeIcon)
