import Vue from 'vue'

import { library, config } from '@fortawesome/fontawesome-svg-core'
// import { faQuestionCircle } from '@fortawesome/pro-light-svg-icons'
// import { faQuestionCircle } from '@fortawesome/pro-regular-svg-icons'
import {
  faPlus,
  faQuestionCircle,
  faExclamationCircle,
  faEnvelope,
} from '@fortawesome/pro-solid-svg-icons'
import {
  faFacebookMessenger,
  faFacebook,
  faWhatsapp,
} from '@fortawesome/free-brands-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

config.autoAddCss = false

library.add([
  faPlus,
  faQuestionCircle,
  faEnvelope,
  faFacebookMessenger,
  faFacebook,
  faWhatsapp,
  faExclamationCircle,
])

Vue.component('fa-icon', FontAwesomeIcon)
