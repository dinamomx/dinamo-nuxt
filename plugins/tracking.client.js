import Vue from 'vue'
import Cookie from 'js-cookie'
import { bootstrap } from 'vue-gtag'

export const COOKIE_NAME = 'DinamoGalleta'
const COOKIE_MAX_AGE = 60 * 60 * 24 * 365
export const ACCEPTED_STATE = 'accepted'
export const DENIED_STATE = 'denied'
export const IMPLICIT_STATE = 'accepted_implicit'
export const INITIAL_VISIT = 'unaccepted'

const state = Vue.observable({ status: 'unknown' })

/**
 * Define el estado del cookie estilo stateMachine.
 *
 * @param  {string} cookie El valor de la cookie
 * @returns {ACCEPTED_STATE | DENIED_STATE | IMPLICIT_STATE | INITIAL_VISIT}
 */
const getCookieStatus = (cookie) => {
  switch (cookie) {
    case ACCEPTED_STATE:
      return ACCEPTED_STATE
    case DENIED_STATE:
      return DENIED_STATE
    case '':
    case undefined:
      return INITIAL_VISIT
    case IMPLICIT_STATE:
    case INITIAL_VISIT: // Si se navega de vuelta lo acepto
      return IMPLICIT_STATE
    default:
      // eslint-disable-next-line no-console
      console.warn(`unknown status of cookie ${cookie}`)
      return IMPLICIT_STATE
  }
}

/**
 * Guarda la cookie en el navegador con el estado de rastreo a guardar en la cookie
 */
const setCookie = () => {
  Cookie.set(COOKIE_NAME, state.status, {
    expires: COOKIE_MAX_AGE,
    sameSite: 'strict',
  })
}
/**
 * Obtiene la cookie del navegador y guarda su estado en la misma
 */
const getCookieFromBrowser = () => {
  const cookie = Cookie.get(COOKIE_NAME)
  state.status = getCookieStatus(cookie)
  if (!cookie) {
    setCookie()
  }
}

getCookieFromBrowser()

/** @type {import('types/vuetracking').TrackingPlugin} */
const tracking = {
  get status() {
    return state.status
  },
  /**
   * Habilita rastreadores de forma explícita (el usuario lo acepto)
   */
  explicitEnable() {
    state.status = ACCEPTED_STATE
    setCookie()
    this.enable()
  },
  /**
   * Habilita rastreadores de forma implicita, (el usuario navegó)
   */
  implictEnable() {
    if (state.status === INITIAL_VISIT) {
      state.status = IMPLICIT_STATE
    }
    setCookie()
    this.enable()
  },
  /**
   * Para ser llamado cuando navegas por primera vez en el sitio
   */
  initialEnable() {
    if (state.status === INITIAL_VISIT) {
      this.enable()
    }
  },
  /**
   * Inicia rastreadores
   *
   * @private
   */
  enable() {
    switch (state.status) {
      case ACCEPTED_STATE:
      case IMPLICIT_STATE:
      case INITIAL_VISIT:
        bootstrap()
        this.$fbpixel?.enable()
        break
      default:
        // eslint-disable-next-line no-console
        console.warn('Este método no lo debes llamar manualmente')
    }
  },
  /**
   * Deshabilita rastreadores y guarda el estado en la cookie
   */
  disableTracking() {
    state.status = DENIED_STATE
    setCookie()
    this.$gtag?.optOut()
    this.$fbpixel?.disable()
  },
}

/**
 * Funcionalidad para el manejo de rastreo.
 * Depende de vue-gtag.
 *
 * @version 1.0
 * @type {import('@nuxt/types').Plugin}
 */
const trackingPlugin = ({ app }, inject) => {
  inject('tracking', tracking)
  app.router?.afterEach((_to, from) => {
    if (from.name && state.status === INITIAL_VISIT) {
      app.$tracking.implictEnable()
    }
  })
  // Arranque inicial de rastreadores
  switch (state.status) {
    case ACCEPTED_STATE:
    case IMPLICIT_STATE:
      app.$tracking.enable()
      break
    default:
    // Si activara por defecto los rastreadores aquí los deshabilitaría
  }
}

export default trackingPlugin
