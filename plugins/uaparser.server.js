import { UAParser } from 'ua-parser-js'
import { SET_BROWSER_DATA } from '~/utils/mutations'

/** @type {import('@nuxt/types').Plugin} */
const middlewareMeta = ({ req, store }) => {
  if (process.static) {return}
  const parser = new UAParser(req.headers['user-agent'])
  store.commit(SET_BROWSER_DATA, parser.getResult())
}

export default middlewareMeta
