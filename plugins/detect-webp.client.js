/* eslint-disable no-param-reassign */
/**
 * Detecta si este navegador soporta webp
 *
 * @param {'basic' | 'lossless'} feature Verificar si soporta basicamente webp o lossless webp
 * @returns {Promise<void>}
 */
function detectWebpSupport(feature = 'basic') {
  const images = {
    basic:
      'data:image/webp;base64,UklGRjIAAABXRUJQVlA4ICYAAACyAgCdASoCAAEALmk0mk0iIiIiIgBoSygABc6zbAAA/v56QAAAAA==',
    lossless:
      'data:image/webp;base64,UklGRh4AAABXRUJQVlA4TBEAAAAvAQAAAAfQ//73v/+BiOh/AAA=',
  }
  return new Promise((resolve, reject) => {
    const image = document.createElement('img')
    image.addEventListener('load', () => {
      if (image.width === 2 && image.height === 1) {
        resolve()
      } else {
        reject()
      }
    })
    image.addEventListener('error', reject)
    image.src = images[feature] || images.basic
  })
}

/** @type {import('@nuxt/types').Plugin} */
const detectWebpPlugin = ({ env }) => {
  detectWebpSupport()
    .then(() => {
      env.hasWebp = true
    })
    .catch(() => {
      env.hasWebp = false
    })
}

export default detectWebpPlugin
