import { debounce } from 'lodash'

/**
 * Monitorea el nivel de scroll del usuario y añade un evento
 * de analíticas cuando sobrepasa la mitad de la página
 */

class ScrollTrack {
  currentScrollPercentage = 0

  tracked = 0

  latestPage = ''

  currentPage = ''

  scrollTrigger = 50

  /**
   * Empieza a monitorear el scroll de la página.
   *
   */
  init() {
    window.addEventListener('scroll', debounce(this.watch.bind(this), 500))
  }

  destroy() {
    window.removeEventListener('scroll', debounce(this.watch.bind(this), 500))
  }

  /**
   * Aplica la diferencia del alto de la pantalla con el scroll actual
   * y envía un evento de anaíticas cuando se sobrepasa la mitad.
   *
   */
  watch() {
    const scrollTop = window.scrollY
    const documentHeight = document.body.offsetHeight
    const winHeight = window.innerHeight
    const scrollPercent = scrollTop / (documentHeight - winHeight)
    const scrollPercentRounded = Math.round(scrollPercent * 100)
    this.currentScrollPercentage = scrollPercentRounded
    if (this.tracked < scrollPercentRounded) {
      this.tracked = scrollPercentRounded
      if (
        scrollPercentRounded > this.scrollTrigger &&
        scrollPercentRounded % 10 === 0
      ) {
        this.trackScrollDepth()
      }
    }
  }

  trackScrollDepth() {
    window.gtag?.('event', 'scroll_depth', {
      eventCategory: 'user-tracking',
      eventAction: 'Scroll Depth',
      eventLabel: document.title,
      eventValue: this.tracked,
    })

    window.$nuxt.$emit('scrollTo', this.tracked)
  }

  /**
   * Resetea el status del tracker
   *
   * @param {import('vue-router').Route} to La ruta a la que vamos
   */
  reset(to) {
    this.latestPage = this.currentPage
    this.currentPage = to.path
    this.tracked = 0
  }
}

/**
 * @type {import('@nuxt/types').Plugin}
 */
const scrollTrackPlugin = ({ app }) => {
  const scrollTrack = new ScrollTrack()
  if (typeof document !== 'undefined') {
    scrollTrack.currentPage = document.location.pathname
    scrollTrack.init()
    app.router?.afterEach((to) => scrollTrack.reset(to))
  }
}

export default scrollTrackPlugin
