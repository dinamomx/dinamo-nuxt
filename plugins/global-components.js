import Vue from 'vue'

import ImageResponsive from '~/components/ImageResponsive.vue'

Vue.component(ImageResponsive.name, ImageResponsive)
