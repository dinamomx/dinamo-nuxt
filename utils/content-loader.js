const { markdown } = require('./markdown')
/**
 * Loader
 *
 * @this {import('webpack').loader.LoaderContext}
 * @param {string} source El archivo fuente
 * @param {import('source-map').RawSourceMap} map - The vue function.
 */
function contentLoader(source, map) {
  if (this.cacheable) {
    this.cacheable()
  }

  this.callback(
    null,
    `module.exports = ${JSON.stringify(markdown(source))}`,
    map
  )
}

module.exports = contentLoader
