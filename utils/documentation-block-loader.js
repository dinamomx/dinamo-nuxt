const markdownIt = require('markdown-it')
/**
 * Loader para etiquetas <docs></docs>.
 *
 * @description Habilita la carga de la etiqueta <docs></docs>
 * para mantener la compatibilidad con vue-docs
 *
 * @this {import('webpack').loader.LoaderContext}
 * @param {string} source El archivo fuente
 * @param {import('source-map').RawSourceMap} map - The vue function.
 */
function documentationBlockLoader(source, map) {
  let output
  if (process.env.NODE_ENV === 'production') {
    output = ''
  } else {
    const markdown = markdownIt({
      html: true,
      linkify: true,
      typographer: true,
    })
    output = `export default function (Component) {
      Component.options.__docs = \`${markdown.render(source)}\`
    }`
  }
  this.callback(null, output, map)
}

module.exports = documentationBlockLoader
