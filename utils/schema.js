/**
 * @typedef {import('schema-dts').WebSite} WebSite
 */
/**
 * @typedef {import('schema-dts').LocalBusiness} LocalBusiness
 */
/**
 * @typedef {import('schema-dts').Organization} Organization
 */

/**
 * Schema.org Structured Data
 *
 * La idea es que este archivo exporte cada uno de los schemas que se
 * usen en toda la página y se importen por separado donde sean necesarios
 *
 * @see https://webcode.tools/json-ld-generator
 * @see https://developers.google.com/search/reference/overview
 * @see https://jsonld.com/
 * @see https://searchengineland.com/schema-markup-structured-data-seo-opportunities-site-type-231077
 * @see https://docs.google.com/spreadsheets/d/1Ed6RmI01rx4UdW40ciWgz2oS_Kx37_-sPi7sba_jC3w/edit#gid=0
 */

const sameAs = [
  'https://www.facebook.com/dinamo.diseno/',
  'https://twitter.com/wdinamo/',
  'https://www.linkedin.com/company/dinamo-mx',
  'https://www.instagram.com/wdinamo/',
]

const commonData = {
  name: 'Dinamo Agencia de Comunicación',
  title: 'Demo Title',
  url: 'http://dinamo.mx',
  description:
    'Expertos en estrategia digital, diseño web, social media, fotografía...',
}
/** @type {import('schema-dts').PostalAddress} */
const address = {
  '@type': 'PostalAddress',
  addressLocality: 'Coyoacán',
  addressRegion: 'CDMX',
  postalCode: '04450',
  streetAddress: 'Erasmo Castellanos Quinto 127',
}
/** @type {import('schema-dts').ImageObject} */
const logo = {
  '@type': 'ImageObject',
  author: 'Dinamo',
  contentLocation: 'Ciudad de Mexico, Mexico',
  contentUrl: 'https://dinamo.mx/logo-dinamo.jpg',
  description: commonData.description,
  name: commonData.name,
}
/** @type {import('schema-dts').GeoCoordinates} */
const geo = {
  '@type': 'GeoCoordinates',
  latitude: '19.345492',
  longitude: '-99.2165958',
}

/** @type {import('schema-dts').WithContext<LocalBusiness>} */
export const localBussiness = {
  '@context': 'https://schema.org',
  '@type': 'LocalBusiness',
  address,
  name: commonData.name,
  description: commonData.description,
  openingHours: ['Mo-Fr 9:00-19:00'],
  telephone: '55 4140 8714',
  url: commonData.url,
  logo,
  image: 'https://dinamo.mx/logo-dinamo.jpg',
  geo,
  hasMap:
    'https://www.google.com.mx/maps/place/Dinamo+agencia+de+comunicaci%C3%B3n/@19.345492,-99.2165958,12.21z/data=!4m8!1m2!2m1!1sdinamo!3m4!1s0x85ce01d6e0f8a7ad:0xbdd6e1bf9dbd8b53!8m2!3d19.334624!4d-99.137503',
  sameAs,
}

/** @type {import('schema-dts').Person[]} */
const founders = [
  {
    '@type': 'Person',
    name: 'Ramses Reyes',
    email: 'ramses@wdinamo.com',
    image: 'https://www.dinamo.mx/files/images/equipo/ramses-center.jpg',
    jobTitle: 'Diseñador UX',
    alumniOf: 'Universidad Autónoma Metropolitana Unidad Xochimilco',
  },
  {
    '@type': 'Person',
    name: 'Seth Gonzalez',
    email: 'seth@wdinamo.com',
    image: 'https://www.dinamo.mx/files/images/equipo/seth-center.jpg',
    jobTitle: 'Comunicación Estratégica',
    alumniOf: 'Universidad Autónoma Metropolitana Unidad Xochimilco',
  },
]

/** @type {import('schema-dts').ContactPoint} */
const contactPoint = {
  '@type': 'ContactPoint',
  contactType: 'Sales',
  telephone: '+52 55 4140 8714',
  email: 'tanya@wdinamo.com',
}

/** @type {import('schema-dts').WithContext<Organization>} */
export const organization = {
  '@context': 'https://schema.org',
  '@type': 'Organization',
  name: commonData.name,
  legalName: commonData.name,
  url: commonData.url,
  logo: 'https://dinamo.mx/logo-dinamo.jpg',
  foundingDate: '2009',
  founders,
  address,
  contactPoint,
  sameAs,
}

/** @type {import('schema-dts').WithContext<WebSite>} */
export const website = {
  '@context': 'https://schema.org',
  '@type': 'WebSite',
  url: commonData.url,
  name: commonData.title,
  author: {
    '@type': 'Organization',
    name: commonData.name,
  },
  description: commonData.description,
  publisher: commonData.name,
}
