const fs = require('fs')
const path = require('path')
const { promisify } = require('util')
const { markdown } = require('./markdown')

const readdir = promisify(fs.readdir)
const readFile = promisify(fs.readFile)

/**
 * Carga un archivo como módulo o usando un loader
 *
 * @param {string} fileName El nombre del archivo
 * @param {string} basePath La ruta al archivo
 */
async function readOrImportFile(fileName, basePath) {
  try {
    if (fileName.match(/\.md/)) {
      const fileContent = await readFile(`${basePath}/${fileName}`, 'utf8')
      return markdown(fileContent)
    }
    return await import(`../${basePath}/${fileName}`)
  } catch (error) {
    // eslint-disable-next-line no-console
    console.warn(`Could not process file ${fileName}`, error)
    return null
  }
}

/**
 * Devuelve una función que obtiene un listado de rutas y sus rutas para generación
 * estática apartir de json o netlify-cms
 *
 * @param {string} basePath La ruta de archivos a escanear (ej: assets/content/blog)
 * @param {string} routeBase La base de la ruta a generar (ej: blog)
 * @returns {Promise<{route: string, payload: any}[]>}
 */
export async function generateRoutes(basePath, routeBase) {
  const files = await readdir(basePath)
  const pathRoutes = await Promise.all(
    files.map(async (file) => {
      let payload = null
      if (file.match(/\.(js|json|mjs|cjs|md)$/)) {
        payload = await readOrImportFile(file, basePath)
      }
      return {
        route: `/${routeBase}/${path.parse(file).name}`, // Return the slug
        payload: payload ? { ...payload } : null,
      }
    })
  )
  return pathRoutes
}
