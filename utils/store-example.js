import Vuex from 'vuex'
import { get } from 'lodash'

const createStore = () =>
  new Vuex.Store({
    state: {
      route: null,
      current: {},
      matched: [],
      currentSlide: 0,
      browserData: null,
    },
    getters: {
      currentTitle: state => {
        let title = state.current.title || state.route
        if (state.current.slides) {
          title = get(state.current, `slides[${state.currentSlide}].title`)
        }
        return title
      },
      currentImage: state => {
        let { image } = state.current
        if (state.current.slides) {
          image = get(state.current, `slides[${state.currentSlide}].image`)
        }
        return image
      },
    },
    mutations: {
      META_CHANGED(state, { route, current, matched }) {
        state.route = route
        state.current = current
        state.matched = matched
      },
      SET_SLIDE(state, index) {
        state.currentSlide = index
      },
      SET_BROWSER_DATA(currentState, parsedUA) {
        currentState.browserData = parsedUA
      },
    },
    actions: {
      nextSlide({ commit, state }) {
        let index = 0
        if (!state.current.slides) {
          index = 0
        } else if (state.current.slides.length <= state.currentSlide + 1) {
          index = 0
        } else {
          index = state.currentSlide + 1
        }
        commit('SET_SLIDE', index)
      },
      prevSlide({ commit, state }) {
        let index = 0
        if (!state.current.slides) {
          index = 0
        } else if (state.currentSlide === 0) {
          index = state.current.slides.length - 1
        } else {
          index = state.currentSlide - 1
        }
        commit('SET_SLIDE', index)
      },
      metaChanged({ commit }, meta) {
        commit('META_CHANGED', meta)
      },

      async nuxtServerInit({ commit }, { req }) {
        const { default: parser } = await import('ua-parser-js')
        if (req && req.headers['user-agent']) {
          commit(
            'SET_BROWSER_DATA',
            Object.freeze(parser(req.headers['user-agent']))
          )
        }
      },
    },
  })

export default createStore
