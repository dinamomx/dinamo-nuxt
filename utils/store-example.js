/* eslint-disable no-param-reassign */
import {
  META_CHANGED,
  SET_BROWSER_DATA,
  SET_BLOG_POSTS,
} from '~/utils/mutations'

/** @typedef {import('types').VuexState} State */

/**
 * @type {import("vuex").GetterTree<State, State>}
 */
export const getters = {}

/**
 * @type {import("vuex").MutationTree<State>}
 */
export const mutations = {
  [META_CHANGED](state, { route, current, matched }) {
    state.route = route
    state.current = current
    state.matched = matched
  },
  [SET_BROWSER_DATA](state, parsedUA) {
    state.browserData = parsedUA
  },
  [SET_BLOG_POSTS](state, list) {
    state.blogPosts = list
  },
}

/**
 * @type {import("vuex").ActionTree<State, State>}
 */
export const actions = {
  metaChanged({ commit }, meta) {
    commit(META_CHANGED, meta)
  },
  nuxtServerInit({ commit }) {
    const files = require.context('~/assets/content/blog/', false, /\.md$/)
    const blogPosts = files.keys().map((key) => {
      const { title, image } = files(key)
      return {
        title,
        image,
        slug: key.slice(2, -5),
      }
    })
    commit(SET_BLOG_POSTS, blogPosts)
  },
}
/** @returns {State} */
export const state = () => ({
  route: null,
  current: {},
  matched: [],
  currentSlide: 0,
  browserData: null,
  blogPosts: [],
})
