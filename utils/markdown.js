const markdownIt = require('markdown-it')
const markdownItAttrs = require('markdown-it-attrs')
const markdownItCheckbox = require('markdown-it-checkbox')
const markdownItContainers = require('markdown-it-container')
const matter = require('gray-matter')
const {
  section,
  column,
  columns,
  notification,
} = require('./markdown-containers')

const renderer = markdownIt({
  html: true,
  linkify: true,
  typographer: true,
})
  .use(markdownItAttrs)
  .use(markdownItCheckbox)
  .use(markdownItContainers, 'section', section)
  .use(markdownItContainers, 'column', column)
  .use(markdownItContainers, 'columns', columns)
  .use(markdownItContainers, 'notification', notification)
/**
 * Interpreta un texto en markdown con frontmatter
 *
 * @param {string} source El contenido del archivo a formatear
 * @returns {{[x: string]: any, $_excerpt?: string, $_content: string}}
 */
function markdown(source) {
  const object = matter(source, {
    excerpt: true,
    // excerpt_separator: '<!-- end -->',
  })

  const result = {
    ...object.data,
    $_excerpt: object.excerpt,
    $_content: renderer.render(object.content),
  }
  return result
}
module.exports = { markdown }
