/**
 * Loader para etiquetas <docs></docs>.
 *
 * @description Habilita la carga de la etiqueta <docs></docs>
 * para mantener la compatibilidad con vue-docs
 *
 * @param {Object} source - The source of the vue Component.
 * @param {Function} map - The vue function.
 */
module.exports = function docsLoader(source, map) {
  let output
  if (process.env.NODE_ENV === 'production') {
    output = ''
  } else {
    output = `export default function (Component) {
      Component.options.__docs = ${JSON.stringify(source)}
    }`
  }
  this.callback(null, output, map)
}
