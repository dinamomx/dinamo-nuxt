/* eslint-disable no-restricted-syntax */
const modularScale = require('modularscale-js')

const ratios = {
  minorSecond: 16 / 15,
  majorSecond: 1.125,
  minorThird: 1.2,
  majorThird: 1.25,
  perfectFourth: 4 / 3,
  augFourth: 1.414,
  perfectFifth: 1.5,
  minorSixth: 1.6,
  goldenSection: 1.61803398875,
  majorSixth: 5 / 3,
  minorSeventh: 16 / 9,
  majorSeventh: 1.875,
  octave: 2,
  majorTenth: 2.5,
  majorEleventh: 8 / 3,
  majorTwelfth: 3,
  doubleOctave: 4,
}

const defaultHeadings = {
  h1: 6,
  h2: 5,
  h3: 4,
  h4: 3,
  h5: 2,
  h6: 1,
  small: -1,
}

/**
 * @typedef {object} IRitmoOption
 * @property {'px' | 'rem' | 'em' } [unit = 'px']
 * @property {number} [baseFontSize = 16]
 * @property {number} [lineHeightFactor = 1.75]
 * @property {number} [lineHeightFraction = 2]
 * @property {?keyof typeof ratios | number} scaleRatio
 * @property {{[size: string]: number}} [headings = typeof defaultHeadings]
 * @property {number[]} [spacings = [1, 2, 3, 4, 5, 6, 8, 10, 12, 14, 16, 20, 24, 32, 48, 64]]
 */

/**
 * Crea tamaños necesarios para el ritmo vertical
 *
 * @param {IRitmoOption} options Los parametros para generar los tamaños
 * @returns {
{
  lineHeights: {
    [x: string]: string;
  },
  fontSizes: {
    [x: string]: string;
  },
  spacings: {
    [x: string]: string;
  },
}
}
 */
function makeRhythm(options) {
  const {
    unit = 'px',
    baseFontSize = 16,
    lineHeightFactor = 1.75,
    lineHeightFraction = 2,
    scaleRatio,
    headings = defaultHeadings,
    spacings = [1, 2, 3, 4, 5, 6, 8, 10, 12, 14, 16, 20, 24, 32, 48, 64],
  } = options
  const baseSize = baseFontSize || 16
  const baseLineHeight = Math.round(baseSize * lineHeightFactor)

  const scale = {
    base: [baseSize],
    ratio:
      (typeof scaleRatio === 'number'
        ? scaleRatio
        : ratios[scaleRatio || 'perfectFourth']) || ratios.perfectFourth,
  }

  const ms = (factor = 0) => modularScale(factor, scale)

  /**
   * Aproxima un tamaño a su absoluto más próximo definidio por una fracción
   * del alto de linea.
   *
   * @param {number} fontSize El tamaño de fuente deseado
   * @param {number} fraction A que fracción aproximarse
   * @returns {number} El resultado
   */
  const proximateLenght = (
    fontSize = baseSize,
    fraction = lineHeightFraction
  ) => {
    const distanceFactor = baseLineHeight / fraction
    let lineHeight = 0
    let multiplier = 1
    if (fontSize <= distanceFactor) {
      lineHeight = distanceFactor
    } else {
      while (multiplier * distanceFactor < fontSize) {
        multiplier += 1
      }
      lineHeight = multiplier * distanceFactor
    }
    return lineHeight
  }

  /**
   * Convierte pixeles en rem
   *
   * @param {number} pixels Pixels
   * @param {string} useUnit La unidad a usar
   * @returns {string}
   */
  function pixelsToRem(pixels = baseSize, useUnit = unit) {
    const rems = pixels / baseSize
    if (Number.isInteger(rems)) {
      return `${rems}${useUnit}`
    }
    return `${rems.toFixed(3)}${useUnit}`
  }

  /**
   * Obtiene un tamaño de pixeles multiplicando una fracción del alto de linea.
   *
   * @param {number} factor El multiplicador
   * @returns {string} El resultado en pixeles
   */
  function lengthByFactor(factor = 0) {
    const value = factor * (baseLineHeight / (lineHeightFraction * 2))
    if (unit === 'em' || unit === 'rem') {
      return pixelsToRem(value, 'rem')
    }
    return `${value}px`
  }

  /**
   * Obtiene el tamaño de fuente deseado basado en el factor
   *
   * @param {number} factor El factor para modular-scale
   * @returns {string} el tamaño de fuente en pixeles o rems
   */
  function getFontSize(factor = 0) {
    const value = ms(factor)
    if (unit === 'rem' || unit === 'em') {
      return pixelsToRem(ms(factor))
    }
    return `${Math.round(value)}px`
  }

  /**
   * Obtiene el alto de linea
   *
   * @param {number} factor El factor de modular-scale corresponde al tamaño
   * @returns {string}
   */
  function getLineHeight(factor = 0) {
    let multiplier = 1
    let value = proximateLenght(multiplier, 1)
    const fontSize = ms(factor)
    while (value < fontSize) {
      multiplier += 1
      value = proximateLenght(multiplier, 1)
    }
    if (unit === 'rem') {
      return pixelsToRem(value)
    }
    if (unit === 'em') {
      return `${value / fontSize}em`
    }
    return `${value}px`
  }

  /** @type {{[x: string]: string}} */
  const lineHeights = {
    base: getLineHeight(0),
    point: lineHeightFactor.toString(),
  }

  /** @type {{[x: string]: string}} */
  const fontSizes = {
    base: `${baseSize}px`,
  }

  for (const [name, size] of Object.entries(headings)) {
    lineHeights[name] = getLineHeight(size)
    fontSizes[name] = getFontSize(size)
  }

  /** @type {{[x: string]: string}} */
  const spacingsResult = {}

  for (const spacing of spacings) {
    spacingsResult[spacing] = lengthByFactor(spacing)
  }

  return {
    lineHeights,
    fontSizes,
    spacings: spacingsResult,
  }
}
module.exports = makeRhythm
