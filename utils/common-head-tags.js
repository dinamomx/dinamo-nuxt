/**
 * Genera la información meta de la página para usarse con vue-meta
 *
 * @param {import('types').SeoObject} data La clave del objeto que contien la información meta
 * @returns {import('vue-meta').MetaInfo}
 */
export function commonHeadTags(data) {
  const { title } = data
  const domain = process.env.PRODUCTION_HOST
  /** @type {import('vue-meta').MetaInfo} */
  const head = {}
  head.meta = []
  if (title) {
    head.title = title
    head.meta.push({ hid: 'og:title', property: 'og:title', content: title })
  }

  /** @type {?string} */
  let ogImage
  let ogImageHeight = ''
  let ogImageWidth = ''
  let ogImageType = 'image/jpeg'
  let ogImageAlt = 'Imágen descriptiva del tema de esta entrada de blog'
  if (data.og_image) {
    if (typeof data.og_image === 'string') {
      ogImage = data.og_image
    } else {
      ogImage = data.og_image.src
      ogImageHeight = data.og_image.height
      ogImageWidth = data.og_image.width
      ogImageType = data.og_image.type || 'image/jpeg'
      ogImageAlt = data.og_image.alt || ogImageAlt
    }
  } else {
    ogImage = data.image_cover || data.image_card || null
  }
  if (ogImage) {
    ogImage = `${domain}/${ogImage}`
    /** @type {import('vue-meta/types/vue-meta').MetaPropertyProperty[]} */
    const ogImageProperties = [
      {
        hid: 'og:image:url',
        property: 'og:image:url',
        content: ogImage,
      },
      {
        hid: 'og:image:type',
        property: 'og:image:type',
        content: ogImageType,
      },
      {
        hid: 'og:image:height',
        property: 'og:image:height',
        content: ogImageHeight,
      },
      {
        hid: 'og:image:width',
        property: 'og:image:width',
        content: ogImageWidth,
      },
      {
        hid: 'og:image:alt',
        property: 'og:image:alt',
        content: ogImageAlt,
      },
    ]
    head.meta = head.meta.concat(ogImageProperties)
  }
  const canonical = `${domain}/${data.permalink}`
  if (canonical) {
    head.link = []
    head.link.push({ rel: 'canonical', href: canonical })
    head.meta.push({
      hid: 'og:url',
      property: 'og:url',
      content: canonical,
    })
  }
  const { description } = data
  if (description) {
    head.meta.push.apply([
      {
        hid: 'og:description',
        property: 'og:description',
        content: description,
      },
      {
        hid: 'description',
        name: 'description',
        content: description,
      },
    ])
  }
  return head
}
