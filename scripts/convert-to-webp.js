#! /usr/bin/node
/* eslint-disable no-restricted-syntax */

/* eslint-disable no-console */
// eslint-disable-next-line import/no-extraneous-dependencies
const { CWebp } = require('cwebp')
const path = require('path')
const fs = require('fs')

const basePath = path.resolve(__dirname, '..')
const staticImages = path.resolve(basePath, 'static/images')
const assetImages = path.resolve(basePath, 'assets/images')

const filter = /(\.jpg)$|(\.jpeg)$|(\.png)$/i

const forcingOverwrite = !!process.argv[2]
/**
 * Recursivamente convierte a webp
 *
 * @param {string} startPath La ruta recursiva
 * @param {typeof convertImage} callback El callback a ejecutar cuando terminte
 */
function fromDirectory(startPath, callback) {
  if (!fs.existsSync(startPath)) {
    return
  }
  const result = {
    fileName: '',
    filePath: startPath,
    fullFileName: '',
  }

  const files = fs.readdirSync(startPath)
  for (const fileName of files) {
    const fullFileName = path.join(startPath, fileName)
    const stat = fs.lstatSync(fullFileName)
    result.fileName = fileName
    result.fullFileName = fullFileName
    if (stat.isDirectory()) {
      fromDirectory(fullFileName, callback) // recurse
    } else if (filter.test(fileName)) {
      callback(result)
    }
  }
}

/**
 *
 * @param {string} file El posible error de que no funcione la conversión
 * @param {Error} [error] El posible error de que no funcione la conversión
 */
const convertImageCallback = (_file, error) => {
  console.log(error || 'Archivo convertido con éxito')
}

/**
 * Convierte la imagen
 *
 * @param {{fileName: string, filePath: string, fullFileName: string}} file El archivo a convertir
 */
function convertImage(file) {
  const { fileName, filePath, fullFileName } = file
  const newName = fileName.replace(filter, '.webp')
  const newFilePath = `${filePath}/${newName}`
  if (fs.existsSync(newFilePath)) {
    console.warn(`Ya existe el archivo ${newFilePath}`)
    if (!forcingOverwrite) {
      console.warn(
        'Abortando, usa el argumento force para sobreescribir archivos'
      )
      return
    }
  }
  console.log(`Converting ${fileName} to ${newName}`)
  const encoder = new CWebp(fullFileName)
  encoder.write(newFilePath, convertImageCallback)
}

fromDirectory(assetImages, convertImage)
fromDirectory(staticImages, convertImage)
