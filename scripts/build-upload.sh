#!/bin/bash
set -eu # Fail on errors or undeclared variables
export $(egrep -v '^#' .env | xargs)
RSYNC_TARGET="$SERVE_USER@$SERVER_HOST:$DEPLOY_ROOT/$PRODUCTION_DOMAIN/current"
echo "BUILD APPLICATION_________________________"
pnpm run build -- --quiet --devtools
echo "UPLOADING BUILT FILES TO $RSYNC_TARGET ___"
rsync -cuah --progress --delete-after .nuxt $RSYNC_TARGET
echo "DONE______________________________________"
