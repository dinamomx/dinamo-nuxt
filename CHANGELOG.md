# Changelog

Todos los cambios notables de este proyecto son registrados aquí sin seguir
ningún standard claro

## (2020-05-30)
- CHANGE: 

## (2020-05-06)
- CHANGE: Cambio del color por defecto de fondo, era demasiado notorio
- BREAKING, FIX: Refactor completo del sistema de cookies y rastreo, ya funciona correctamente.
- CHANGE: No injectar en runtime los estilos de fontawesome
- CHANGE: Actualización de tailwind a 1.4.4
  - Esta versión de tailwind trae entre otras cosas, colores transparentes.
- FIX: No purgar los estilos base de html y tipografía

## (2020-04-15)

- BREAKING: Nueva versión de
  [nuxt](https://nuxtjs.org/guide/release-notes#v2.12.0)
  - Nueva forma de uso programático puede requerir cambios en sitios que usen
    ssr con express
  - Nueva sintaxis de [fetch](https://nuxtjs.org/api/pages-fetch) Puede romper
    sitios que usen store para peticiones a api, particularmente andres siegel y
    toyota. Solo lo veo útil en componentes aislados
- BREAKING: Nuevos botones basados en un artículo nuevo. Usos más genéricos y
  fáciles reusar o extender
- BREAKING: Mi intento de hacer inputs es un fracaso parcial. Se opta por
  [@tailwind/custom-forms](https://tailwindcss-custom-forms.netlify.com/)
- FIX: Hacer cambios en el index para demostraciones costaba más de lo que valía
  la pena asį que se opto por tener una carpeta dedicada a demostraciones y
  ejemplos de uso de componentes
- FEAT: La fuente [Inter](https://rsms.me/inter/) se vuelve la default y usa
  instalación local
- FIX: Limpieza de archivo de estilos html para solo aplicar los cambios
  necesarios para ritmo verical
- CHANGE: El fondo ya no es blanco y el texto ya no es negro por default

## (2020-03-02)

- FEAT: Añadiendo netlify-cms, se accede desde `/admin/`
- FIX: Limpiando archivos json de datos sin usar sobre las cookies
- FEAT: Añadiendo `BUILD_DRAFT` a las variables de entorno para construir si o
  si las páginas marcadas como **DRAFT**
- FEAT: Preconfiguración de netlify con un robots para evitar conflictos de SEO
- WIP: Boceto de configuración de sitio en `assets/data/site_settings.json`
  editable desde netlify-cms
- FEAT: Ejemplo de blog con markdown y netlify-cms
- FIX: Doble imagen en card cuando se pasa un enlace como prop
- FIX: HeroFeneric Le faltaba el scrim y un fallback para cuando la imagen no
  está optimizada para distintos dispositivos

## (2020-02-11)

- FIX: Corrección de `normalSrc` en componente `<ImageResponsive>` que daba el
  size de la imagen con la url de la imagen
- FIX: Permisos y hashbang en `scripts/convertToWebp.js` que prevenia de
  ejecutar `convert:images`

## (2020-02-06)

- FIX: Corrección del runtime `generate` usando `process.static` para evitar
  errores cuando se busca el request del lado del server
- FIX: Evitar css duplicado en el código al no añadir manualmente el css de
  fontawesome y permitir que el plugin lo haga por si mismo
- FIX: Añadir a lista blanca los estilos del burguer del menu
- FEAT: Detección de soporte para webp
- CHORE: Actualización de dependencias
- FEAT: El comentario `/* DRAFT */` de cada página evita que se compile una
  página vue en particular
