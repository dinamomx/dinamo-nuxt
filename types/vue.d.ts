import Vue from 'vue'
import { TrackingPlugin } from './vuetracking'

interface FacebookPixel {
  event: (name: string, data?: any) => void
  query: (...args: any[]) => void
  enable: () => void
  disable: () => void
  init: (id: string, data?: any) => void
}

declare module 'vue/types/vue' {
  interface Vue {
    $fbpixel: FacebookPixel
    $tracking: TrackingPlugin
  }
}
