export type TrackingPlugin = {
  readonly status: string
  /**
   * Habilita rastreadores de forma explícita (el usuario lo acepto)
   */
  explicitEnable(): void
  /**
   * Habilita rastreadores de forma implicita, (el usuario navegó)
   */
  implictEnable(): void
  /**
   * Para ser llamado cuando navegas por primera vez en el sitio
   */
  initialEnable(): void
  /**
   * Inicia rastreadores
   *
   * @private
   */
  enable(): void
  /**
   * Deshabilita rastreadores y guarda el estado en la cookie
   */
  disableTracking(): void
}
