import './gdocs-leads'
import './vue-meta'
import './vue'

export interface ImageObject {
  route: string
  name: string
  format: string
}
export type ogImage =
  | string
  | { src: string; height: string; width: string; type: string; alt: string }
export interface SeoObject {
  title: string | undefined
  domain: string
  og_image?: ogImage
  image_cover: string
  image_card?: string
  images_content?: string[]
  permalink: string
  description: string
}

/**
 * Base para todo el contenido generado con markdown
 */
export interface MarkdownContent {
  /** El contenido en html */
  $_content: string
  /** Un texto sin html haciendo un resumen del contenido */
  $_excerpt?: string
}

export interface Checklist extends MarkdownContent {
  title: string
}

export type RouteMeta = {
  // Aquí van las opciones meta que quieras meter como meta para el componente
  [x: string]: boolean
}

export interface VuexModuleMeta {
  route: string | null
  current: RouteMeta
  matched: RouteMeta[]
}

export interface BlogPost extends MarkdownContent {
  slug?: string
  title: string
  image: string
}
export interface VuexState extends VuexModuleMeta {
  currentSlide: number
  browserData: IUAParser.IResult | null
  blogPosts?: BlogPost[]
}
