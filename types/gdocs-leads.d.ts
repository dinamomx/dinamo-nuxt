declare namespace FormLeads {
  interface RemoteForm {
    debugMail: string[]
    mailTitle: string[]
    sendTo: string[]
    subject: string[]
    sheetDestiny: string[]
    senderName: string[]
    Fecha: string[]
    ocultCopyTo: string[]
    formDataOrder: string[]
    [x: string]: string[]
  }

  interface DebugDataMail {
    sendTo: string[]
    copyTo: string
    ocultCopyTo: string[]
    subject: string[]
    replyTo: string
    form: RemoteForm
  }

  interface DebugDataSheet {
    fileName: string
    url: string
    form: RemoteForm
  }

  interface ScriptResult<T> {
    status: boolean
    errorData: string
    debugData: {
      message: string
      data: T
      timestamp: Date
    }
  }

  export interface Debug {
    message: string
    mail: ScriptResult<DebugDataMail>
    sheet: ScriptResult<DebugDataSheet>
    timestamp: Date
  }

  export interface Response {
    result: boolean
    saveResult: ScriptResult<DebugDataMail>
    mailResult: ScriptResult<DebugDataSheet>
  }

  type PayloadType = Date | string | boolean | number
  export interface Payload {
    sheetDestiny: string
    formDataOrder: string | string[]
    sendTo: string
    replyTo: string
    subject: string
    debugMail: string
    senderName: string
    ocultCopyTo: string
    copyTo: string
    mailTitle: string
    [x: string]: PayloadType | PayloadType[]
  }
}
