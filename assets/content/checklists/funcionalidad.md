---
title: Funcionalidad y rendimiento
---

### Pruebas Manuales

Hay dos formas de ejecutar el proyecto en producción y todas las pruebas a
continuación deben ser ejecutadas en este modo localmente a menos que se indique
lo contrario.

En caso de que el proyecto se vaya a subir en modo estático (generate):

```bash
pnpm run generate
hs dist
```

Y para cuando el proyecto se ejecutara en node (build):

```bash
NODE_ENV=production pnpm run build
NODE_ENV=production pnpm run start
```

> Todas las pruebas deben ejecutarse en los navegadores especificados con
> dispositivos reales sin excepción

> Para apoyarse con el debug en dispositivos móbiles hay que usar "remote
> debugging"
> [Chrome](https://developers.google.com/web/tools/chrome-devtools/remote-debugging),
> [Firefox](https://developer.mozilla.org/es/docs/Tools/Remote_Debugging/Firefox_para_Android)
> y safari
> [Safari](https://appletoolbox.com/use-web-inspector-debug-mobile-safari/)

- [ ] Hacer pruebas coordinadas con el cliente de todos los formularios de la
      página
  - [ ] Safari
  - [ ] Safari IOS
  - [ ] Firefox
  - [ ] Chrome
  - [ ] Chrome IOS
  - [ ] Edge
- [ ] Verificar que las clases que se usan dinámicamente o de terceros no sean
      eliminadas por [purgecss](https://www.purgecss.com/configuration) y
      agregarlos en lista blanca de ser necesario.
  - [ ] Safari
  - [ ] Safari IOS
  - [ ] Firefox
  - [ ] Chrome
  - [ ] Chrome IOS
  - [ ] Edge
- [ ] Asegurarse que la información del formulario que llega por correo sea
      relevante en orden adecuado y filtrando "undefined"
- [ ] Revisar que existan los tamaños de imagenes necesarios para cada pantalla
      y su versión en webp así como usados (en el componente ImageResponsive) y
      que carguen correctamente según sea posible, por ejemplo safari no soporta
      el formato webp así que estaría cargando las imagenes en jpg. La mejor
      forma para confirmar que imagen es cargada es usando la pestaña de "Red"
      en el panel de desarrollo.
  - [ ] Safari
  - [ ] Safari IOS
  - [ ] Firefox
  - [ ] Chrome
  - [ ] Chrome Android
  - [ ] Chrome IOS
  - [ ] Edge
- [ ] Confirmar que todos los controles interactivos funcionen como esperado en
      todas las pantallas, ej: el menú de hamburguesa. **De forma óptima
      listarlos en este documento**
  - [ ] Safari
  - [ ] Safari IOS
  - [ ] Firefox
  - [ ] Chrome
  - [ ] Chrome Android
  - [ ] Chrome IOS
  - [ ] Edge
- [ ] Asegurarse de proveer fallbacks de estilos o funcionalidades pesadas como
      parallax y css filter en dispositivos móbiles.
  - [ ] Safari IOS
  - [ ] Chrome Android
  - [ ] Chrome IOS

### Otras pruebas

Estas pruebas se tienen que ejecutar ya en el dominio de producción.

- [ ] Confirmar que el Service Worker esté comportándose como esperado
      invalidando cache apropiadamente y permitiendo ver la página en modo
      offline.
- [ ] Usar Lighthouse para calificar el rendimiento de la página, hay que
      acercarse lo más posible al 90%.
- [ ] Comprobar que operaciones costosas no se ejecuten de forma redundate (como
      solicitudes ajax) y que grandes sets de datos no se guarden en memoria de
      forma indefinida (como cache de solicitudes ajax).
- [ ] Comprobar que en caso de potenciales errores (api caída o fallando ).
- [ ] Eliminar en medida de lo posible todos los comentarios `eslint-disable`
      del código (esas reglas están por una razón).
