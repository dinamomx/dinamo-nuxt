---
title: SEO
---

Recuerda que para todas las etiquetas que estarán en el head tenemos dos
opciones de agregarlas, en `app.html` o en `nuxt.config.js`.  
Si tienes duda de en donde es mejor ponerlas, responde esta pregunta **¿Esta
etiqueta puede cambiar a lo largo de la página?**

- **Si** cambiará el contenido de la etiqueta, va en`nuxt.config.js`
- **No** cambiará el contenido de la etiqueta, va en`app.html`

1. [ ] La página tiene el título definidio en la constante `head` de nuxt.config
2. [ ] Se definió una descripción global en la constante `head` de nuxt.config
3. [ ] Se agregó el favicon ya sea reemplazando `static/icon.png` o agregando
       las etiquetas correspondientes en el head.
       [Generador de íconos](https://realfavicongenerator.net/)
4. [ ] Se asignó el meta fb:pages en el head con la id de la página de facebook
       del negocio.
5. [ ] Se genero la imagen "og:image" para mostrar en redes sociales.
6. [ ] Se creo una cuenta de google analytics y se enlazó en la página. Esto se
       logra modificando la variable de entorno `GA_ID` para reemplazar la ID de
       la plantilla por la de la página
7. [ ] Hay eventos (de analíticas) significativos en la página, como al hacer
       click en un enlace de whatsapp, envío de formulario o acción de compra.
8. [ ] Se enlazó la cuenta de adwords apropiada y se activaron, las conversiones
       de forma apropiada. (Esto es opcional)
9. [ ] Se enlazó el pixel facebook apropiado y se activaron, las conversiones de
       forma apropiada. (Esto es opcional)
10. [ ] Se creó y enlazó la cuenta de hotjar (opcional). Con esta herramienta
        tendrías que agregar el script en `app.html`
11. [ ] Se conectó google webmaster tools, el método de etiqueta es recomendado.
        Asegurarse de que estén añadidas todas las posibles combinaciones de
        https y www
12. [ ] Se completó la configuración de manifest, ya sea manual o en nuxt.config
        [Referencia](https://pwa.nuxtjs.org/)
13. [ ] Comprobaste el rendimiento de la página ya en producción y con
        certificado usando
        [PageSpeed insights](https://developers.google.com/speed/pagespeed/insights/)
14. [ ] Crear y enlazar el sitemap (creación manual)
15. [ ] Verificar enlaces rotos
16. [ ] Auditar todo el sitio y sus páginas con (Seo
        Optimer)[https://www.seoptimer.com]
17. [ ] Revisar ortografía, de preferencia solicitar ayuda a los dones del copy.
18. [ ] Verificar que el sitio esté redirigiendo correctamente a https://www
        (apoyarse en `curl` para ello)
19. [ ] También utilizar la nueva (web.dev)[https://web.dev/measure] para
        optimizar el sitio

### Semántica

- [ ] Siempre asegurarse de solo tener un solo elemento `<main>`
- [ ] No tener `<section>` dentro de otro `<section>`
- [ ] Tener un `<header>` y un `<footer>`
- [ ] Usar `<nav>` para los enlaces en el `<header>` y `<footer>`
- [ ] Tener solo un h1 en la página (la etiqueta <article> es exenta ), y que
      los demás títulos funcionen en arbol, ej. no tener un h3 después de un h4
- [ ] `section` no va dentro de `main`
- [ ] Comprobar la semántica de todas las páginas con
      [construct.css](https://t7.github.io/construct.css/)
