/* eslint-disable no-param-reassign */
import Vue from 'vue'
import path from 'path'
import { remove } from 'fs-extra'
import consola from 'consola'
import { getNuxtConfig } from '@nuxt/config'
import { Nuxt } from '@nuxt/core'
import { Builder } from '@nuxt/builder'

export type VueClass<V extends Vue> = (new (...args: any[]) => V) & typeof Vue

const rootDirectory = path.resolve(__dirname, '..', '..', '..')

type TemplateType = {
  src: string
  dst: string
  custom?: string | false
}

type StringOrTemplateType = string | TemplateType

export async function compileTemplate(
  template: StringOrTemplateType,
  destination?: unknown
): Promise<string | false>

export async function compileTemplate(
  template: StringOrTemplateType[],
  destination?: unknown
): Promise<string[] | false>

export async function compileTemplate(
  template: StringOrTemplateType[] | StringOrTemplateType,
  destination?: unknown,
  options: unknown = {}
): Promise<string[] | string | false> {
  if (arguments.length < 3) {
    options = destination || {}
    destination = undefined
  }

  const config = getNuxtConfig(options)

  config.rootDir = rootDirectory
  config.dev = false
  config.test = false
  config.server = false

  const nuxt = new Nuxt(config)
  const builder = new Builder(nuxt)

  const templateContext: {
    templateFiles: TemplateType[]
  } = builder.createTemplateContext()

  let templates: StringOrTemplateType[] = Array.isArray(template)
    ? (template as StringOrTemplateType[])
    : ([template] as StringOrTemplateType[])

  templateContext.templateFiles = templates.map((singleTempalte) => {
    if (typeof singleTempalte === 'string') {
      return {
        src: require.resolve(singleTempalte),
        dst: path.join(
          rootDirectory,
          '.nuxt',
          (destination as string) || path.basename(singleTempalte)
        ),
        custom: false,
      }
    }

    return {
      src: path.resolve(rootDirectory, '../template', singleTempalte.src),
      dst: path.join(rootDirectory, '.nuxt', singleTempalte.dst),
      custom: singleTempalte.custom,
    }
  })

  try {
    // clear all destinations
    await Promise.all(
      templateContext.templateFiles.map(({ dst }: TemplateType) => remove(dst))
    )

    await builder.compileTemplates(templateContext)

    if (Array.isArray(template)) {
      return templateContext.templateFiles.map(
        (singleTempalte) => singleTempalte.dst
      )
    }

    const [templateWithContext] = templateContext.templateFiles
    return templateWithContext.dst
  } catch (error) {
    consola.error('Could not compile template', error.message, template)
    return false
  }
}

export function importComponent(componentPath: string): Promise<VueClass<Vue>> {
  return import(componentPath).then((m) => m.default || m)
}

export const vmTick = (vm: Vue): Promise<null> => {
  return new Promise((resolve) => {
    vm.$nextTick(resolve)
  })
}
