/**
 * @jest-environment jsdom
 */
import Vue, { VueConstructor } from 'vue'
import { mount, createLocalVue } from '@vue/test-utils'
import { renderToString } from '@vue/server-test-utils'
import AlertDevelopment from '~/components/AlertDevelopment.vue'

describe('AlertDevelopment.vue', () => {
  let localVue: VueConstructor<Vue> = createLocalVue()
  let Component = AlertDevelopment
  // Mock domain
  const mockedProductionDomain = new URL(
    '/some-random/nested/route',
    'http://www.example.com'
  )
  const originalLocation = window.location

  beforeAll(() => {
    delete window.location
    global.window = Object.create(window)
    Object.defineProperty(global.window, 'location', {
      value: mockedProductionDomain,
      writable: true,
    })
    localVue = createLocalVue()
  })

  afterAll(() => {
    window.location = originalLocation
  })
  it('Does not render if domain matches on server', async () => {
    const wrapper = await renderToString(Component, {
      localVue,
      propsData: {
        productionHost: mockedProductionDomain.host,
      },
      mocks: {
        $nuxt: {
          context: {
            req: {
              headers: {
                host: mockedProductionDomain.host,
              },
            },
          },
        },
      },
    })
    expect(wrapper).toBe('<!---->')
  })
  it('Renders if domain does not match on server', async () => {
    const wrapper = await renderToString(Component, {
      localVue,
      propsData: {
        productionHost: 'notthedomain.com',
      },
      mocks: {
        $nuxt: {
          context: {
            req: {
              headers: {
                host: mockedProductionDomain.host,
              },
            },
          },
        },
      },
    })
    expect(wrapper).not.toBe('<!---->')
  })
  it('does not render on client if domain matches', () => {
    const wrapper = mount(Component, {
      localVue,
      propsData: {
        productionHost: mockedProductionDomain.host,
      },
    })
    expect(() => wrapper.get('.alert-dev')).toThrow('')
  })
  it("Renders when domain doesn't matchs on client", () => {
    const wrapper = mount(Component, {
      localVue,
      propsData: {
        productionHost: 'notthedomain.com',
      },
    })
    expect(wrapper.find('.alert-dev').element).toBeTruthy()
  })
})
