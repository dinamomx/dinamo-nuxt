describe('AlertCookie', () => {
  beforeEach(() => {
    cy.visit('/')
  })
  it('Shows alert cookie on first navigation', () => {
    cy.getCookie('DinamoGalleta').should('be', 'unaccepted')
    cy.get('.alert-cookie')
      .as('AlertCookie')
      .should('be.visible')
      .should('contain', 'Uso de cookies')
  })

  it('Sets cookie accepted cookie on accept', () => {
    cy.get('.alert-cookie')
      .as('AlertCookie')
      .find('button')
      .should('be.visible')
      .click()
      .wait(500)
      .then(() => {
        cy.get('@AlertCookie').should('not.exist')
        cy.getCookie('DinamoGalleta').should('be', 'accepted')
      })
  })

  it('Should accept implicit if i just navigate', () => {
    cy.getCookie('DinamoGalleta').should('be', 'unnacepted')
    cy.visit('/blog')
      .wait(500)
      .then(() => {
        cy.getCookie('DinamoGalleta').should('be', 'implicit')
        cy.get('.alert-cookie').should('not.exist')
      })
  })
})
