/** @type {import('eslint').Linter.Config} */
const config = {
  root: true,
  env: {
    es6: true,
    browser: true,
    node: true,
  },
  globals: {
    Vue: true,
  },
  overrides: [
    {
      files: ['*.ts', '*.tsx'],
      rules: {
        '@typescript-eslint/explicit-function-return-type': ['warn'],
      },
    },

    {
      files: [
        '**/__tests__/*.{j,t}s?(x)',
        '**/tests/unit/**/*.spec.{j,t}s?(x)',
      ],
      env: {
        jest: true,
      },
    },
    {
      files: ['store/**/*.{js,ts}'],
      rules: {
        'no-param-reassign': 'off',
      },
    },
  ],
  reportUnusedDisableDirectives: true,
  parser: 'vue-eslint-parser',
  parserOptions: {
    parser: '@typescript-eslint/parser',
    sourceType: 'module',
    extraFileExtensions: ['.vue'],
    ecmaVersion: 2020,
  },
  extends: [
    'airbnb-base',
    'standard',
    'plugin:unicorn/recommended',
    'plugin:import/errors',
    'plugin:import/warnings',
    'plugin:import/typescript',
    'plugin:vue/vue3-recommended',
    'plugin:@typescript-eslint/recommended',
    'prettier',
    'prettier/standard',
    'prettier/unicorn',
    'prettier/vue',
    'prettier/@typescript-eslint',
  ],
  plugins: ['jest', 'unicorn', 'vue'],
  settings: {
    'import/resolver': {
      node: { extensions: ['.js', '.mjs', '.ts', '.vue', '.tsx', '.vue'] },
    },
  },
  rules: {
    /**********************/
    /* General Code Rules */
    /**********************/

    // Prefiero no defaults
    'import/prefer-default-export': 'off',

    // No me importa si tiene o no extensiones el archivo
    'import/extensions': 'off',

    // Se reemplaza por typescript
    'no-unused-vars': 'off',
    // Enforce import order
    'import/order': 'error',

    // Imports should come first
    'import/first': 'error',

    // Other import rules
    'import/no-mutable-exports': 'error',

    // Allow unresolved imports
    'import/no-unresolved': 'off',

    // Allow async-await
    'generator-star-spacing': 'off',

    // Allow debugger during development
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'warn',
    'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'warn',

    // Prefer const over let
    'prefer-const': ['off'],

    // No single if in an "else" block
    'no-lonely-if': 'error',

    // Force curly braces for control flow,
    // including if blocks with a single statement
    curly: ['error', 'all'],

    // No async function without await
    'require-await': 'error',

    // Force dot notation when possible
    'dot-notation': 'error',

    'no-var': 'error',

    // Force object shorthand where possible
    'object-shorthand': 'error',

    // No useless destructuring/importing/exporting renames
    'no-useless-rename': 'error',

    // Replaced by its typescript equivalent
    'no-unused-expressions': 'off',

    'no-plusplus': ['error', { allowForLoopAfterthoughts: true }],
    camelcase: 'off',

    /**********************/
    /*   Unicorn Rules    */
    /**********************/

    // Pass error message when throwing errors
    'unicorn/error-message': 'error',

    // Uppercase regex escapes
    'unicorn/escape-case': 'error',

    // Array.isArray instead of instanceof
    'unicorn/no-array-instanceof': 'error',

    // Prevent deprecated `new Buffer()`
    'unicorn/no-new-buffer': 'error',

    // Keep regex literals safe!
    'unicorn/no-unsafe-regex': 'off',

    // Lowercase number formatting for octal, hex, binary (0x12 instead of 0X12)
    'unicorn/number-literal-case': 'error',

    // ** instead of Math.pow()
    'unicorn/prefer-exponentiation-operator': 'error',

    // includes over indexOf when checking for existence
    'unicorn/prefer-includes': 'error',

    // String methods startsWith/endsWith instead of more complicated stuff
    'unicorn/prefer-starts-ends-with': 'error',

    // textContent instead of innerText
    'unicorn/prefer-text-content': 'error',

    // Enforce throwing type error when throwing error while checking typeof
    'unicorn/prefer-type-error': 'error',

    // Use new when throwing error
    'unicorn/throw-new-error': 'error',

    // Only tow file conventions
    'unicorn/filename-case': [
      'error',
      {
        cases: {
          kebabCase: true,
          pascalCase: true,
        },
      },
    ],

    // I actually like nulls
    'unicorn/no-null': 'off',

    /**********************/
    /*     Vue Rules      */
    /**********************/

    // Disable template errors regarding invalid end tags
    'vue/no-parsing-error': [
      'error',
      {
        'x-invalid-end-tag': false,
      },
    ],

    // Maximum 5 attributes per line instead of one
    'vue/max-attributes-per-line': 'off',

    'vue/component-tags-order': [
      'error',
      { order: ['docs', 'template', 'script', 'style'] },
    ],

    /**********************/
    /*  Typescript Rules  */
    /**********************/
    // Reemplaza el nativo para entender la sintaxis de tpyescript
    '@typescript-eslint/no-unused-expressions': 'error',

    '@typescript-eslint/no-unused-vars': [
      'error',
      { args: 'all', argsIgnorePattern: '^_' },
    ],

    // Siempre usa import ya que permite el análisis estático de dependencias
    '@typescript-eslint/no-var-requires': 'off',

    // Useless when ussing apis
    '@typescript-eslint/camelcase': 'off',

    // No funciona correctamente en javascript
    '@typescript-eslint/explicit-function-return-type': ['off'],

    // No funciona en javascript
    '@typescript-eslint/explicit-module-boundary-types': 'off',
  },
}

module.exports = config
