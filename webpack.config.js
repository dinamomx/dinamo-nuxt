const resolve = require('path').resolve; // eslint-disable-line

// Minimal Webpack config to supply to Eslint.
// This is not actually used by Nuxt but instead mirrors
// the resolve and loader rules.
module.exports = {
  resolve: {
    extensions: ['.wasm', '.mjs', '.js', '.json', '.vue', '.jsx'],
    alias: {
      '~': resolve(__dirname, ''),
      '~~': resolve(__dirname, ''),
      '@': resolve(__dirname, ''),
      '@@': resolve(__dirname, ''),
      assets: resolve(__dirname, 'assets'),
      static: resolve(__dirname, 'static'),
    },
    modules: [
      'node_modules',
      resolve(__dirname, 'node_modules'),
      resolve(__dirname, 'nuxt/node_modules'),
    ],
  },

  module: {
    rules: [
      {
        test: /\.(png|jpe?g|gif|svg)$/,
        loader: 'url-loader',
        options: { limit: 1000, name: 'img/[name].[hash:7].[ext]' },
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
      },
      {
        test: /\.vue$/,
        exclude: /node_modules/,
        loader: 'vue-loader',
      },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader', 'sass-loader'],
      },
      {
        test: /\.scss$/,
        use: ['style-loader', 'css-loader', 'sass-loader'],
      },
    ],
  },
}
