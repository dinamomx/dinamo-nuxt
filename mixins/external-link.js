import Vue from 'vue'

/**
 * ExternalLink
 * Dicierne si la propiedad href es un enlace externo o no
 *
 * @mixin
 * @version 1.0
 * @since 03-08-08
 */

export const externalLinkMixin = Vue.extend({
  props: {
    external: {
      type: Boolean,
      default: false,
    },
    href: {
      type: [String, Object],
      required: true,
      default: '/',
    },
  },
  computed: {
    /** @returns {boolean} */
    isExternalLink() {
      if (this.external) {
        return true
      }
      if (typeof this.href === 'string') {
        return (
          this.href.startsWith('http') ||
          this.href.startsWith('//') ||
          this.href.startsWith('#')
        )
      }
      return false
    },
  },
})
