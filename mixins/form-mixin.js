import Vue from 'vue'

/**
 * Mixin de formulario genérico
 * Tiene el objetivo de permitir la reusabilidad
 * de la funcionalidad de envio de leads
 *
 * @mixin FormMixin
 * @version 1.8
 *
 * En la versión 1.7 se espera que el componente de el feedback o rediriga una
 * landing page
 *
 * En la versión 1.8 se cambiaron los boleanos de estado por un enum
 */

export default Vue.extend({
  props: {
    /**
     * Url de envío
     */
    endpoint: {
      type: String,
      default:
        'https://script.google.com/macros/s/AKfycbww4UOBz5ftnNclXIpKAQUGqx-9jjIcGUkWdtALEUu06AnAkWs/exec',
    },
    /**
     * Título del mail
     */
    mailTitle: {
      type: String,
      default: 'Prueba de la plantilla',
    },
    /**
     * Hoja del drive a usar
     */
    sheetDestiny: {
      type: String,
      default: 'Leads',
    },
    /**
     * Orden en el que se muestran los datos en el correo,
     *   lo que no esté incluido aquí es omitido
     */
    /** @type {import('vue').PropOptions<string[]>} */
    formDataOrder: {
      type: Array,
      default() {
        return ['Fecha', 'Nombre', 'Teléfono', 'Correo']
      },
    },
    /**
     * Correo al que se envía este lead
     */
    sendTo: {
      type: String,
      default: 'cesar@wdinamo.com',
    },
    /**
     * Correo al que las respuestas son enviadas
     */
    replyTo: {
      type: String,
      default: '',
    },
    /**
     * Asunto del correo
     */
    subject: {
      type: String,
      default: 'Formulario de contacto en la plantilla de dinamo',
    },
    /**
     * Nombre del remitente
     */
    senderName: {
      type: String,
      default: 'Dinamo',
    },
    /**
     * Con copia
     */
    copyTo: {
      type: String,
      default: '',
    },
    /**
     * Copia oculta
     */
    ocultCopyTo: {
      type: String,
      default: '',
    },
    /**
     * Correo al que se envía las notificaciones de fallos.
     */
    debugMail: {
      type: String,
      default: 'cesar@wdinamo.com',
    },
  },
  data: () => ({
    /**
     * El estado del formulario
     * @see https://kentcdodds.com/blog/stop-using-isloading-booleans/
     * @type {'idle' | 'pending' | 'resolved' | 'error'}
     */
    state: 'idle',
    // Modelo del formulario
    /** @type {{[x: string]: Date | string | boolean | number | object}} */
    model: {
      Fecha: '',
      Nombre: '',
      Teléfono: '',
      Correo: '',
      // Campo de terminos de privacidad
      'Aviso de privacidad': false,
    },
  }),
  computed: {
    /**
     * El objeto a enviarse, combina las props con el
     * objeto del formulario.
     *
     * @returns {FormLeads.Payload} Payload.
     */
    payload() {
      return {
        sheetDestiny: this.sheetDestiny,
        formDataOrder: this.formDataOrder,
        sendTo: this.sendTo,
        replyTo: this.replyTo,
        subject: this.subject,
        debugMail: this.debugMail,
        senderName: this.senderName,
        ocultCopyTo: this.ocultCopyTo,
        copyTo: this.copyTo,
        mailTitle: this.mailTitle,
        ...this.model,
      }
    },
  },
  methods: {
    /**
     * Procesa el resultado de la respuesta.
     *
     * @param {FormLeads.Response} r - La respuesta del script de gdocs.
     */
    handleResponse({ result, mailResult }) {
      if (result) {
        this.isSuccess()
      } else if (mailResult.status) {
        this.state = 'resolved'
        // eslint-disable-next-line no-console
        console.warn('No se guardó en el drive, pero se envió al correo')
        this.isSuccess()
      } else {
        this.state = 'error'
        // eslint-disable-next-line no-console
        console.error('Algo malo sucede')
        this.isError()
      }
    },
    /**
     * Callback para mostrar resultado de éxito.
     *
     */
    isSuccess() {
      /**
       * Evento de envio exitoso, se puede usar para cerrar modales
       *
       * @event success
       * @type {Event}
       * @param {object} payload - La payload
       */
      this.$emit('success', this.payload)
      this.resetForm()
    },
    /**
     * Callback para mostrar error.
     *
     */
    isError() {
      /**
       * Evento de error enviando, se puede usar para cerrar modales
       *
       * @event error
       * @type {Event}
       */
      this.$emit('error')
    },
    setTodayDate() {
      this.model.Fecha = new Date().toLocaleString('es')
    },
    /**
     * Resetea los datos del formulario.
     *
     */
    resetForm() {
      Object.keys(this.model).forEach((key) => {
        if (this.payload[key] instanceof Date) {
          this.model[key] = new Date()
        } else {
          this.model[key] = ''
        }
      })
      this.setTodayDate()
    },
    /**
     * Convierte los datos del formulario en FormData.
     *
     * @returns {URLSearchParams} FormData.
     */
    formatPayload() {
      const formData = new URLSearchParams()
      Object.keys(this.payload).forEach((key) => {
        const value = this.payload[key]
        if (value instanceof Date) {
          formData.append(key, value.toLocaleString('es'))
        } else if (typeof value === 'object') {
          formData.append(key, JSON.stringify(value))
        } else if (typeof value === 'string' && value) {
          formData.append(key, value)
        } else if (typeof value === 'number' && value) {
          formData.append(key, value.toString())
        }
      })
      return formData
    },
    /**
     * Envía el formulario al script de gdocs.
     *
     * @returns {Promise<void>} La promesa de la respuesta
     *
     */
    formSend() {
      this.state = 'pending'
      this.setTodayDate()
      const formData = this.formatPayload()
      return fetch(this.endpoint, {
        method: 'POST',
        mode: 'cors',
        body: formData,
      })
        .then((response) => response.json())
        .then((data) => {
          this.handleResponse(data)
        })
        .catch((error) => {
          // eslint-disable-next-line no-console
          console.error(error)
          this.state = 'error'
          this.isError()
        })
    },
  },
})
