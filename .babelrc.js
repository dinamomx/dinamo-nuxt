/**
 * @type {import('@babel/core').ConfigFunction}
 */
const babelConfig = ({ env, cache }) => {
  cache(() => process.env.NODE_ENV === 'production')
  return {
    env: {
      test: {
        presets: [
          [
            '@babel/preset-env',
            {
              targets: {
                node: 'current',
              },
            },
          ],
        ],
      },
    },
    plugins: [
      '@babel/plugin-proposal-optional-chaining',
      'lodash',
      '@babel/plugin-proposal-class-properties',
    ],
  }
}

module.exports = babelConfig
