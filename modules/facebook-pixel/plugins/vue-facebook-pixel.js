/* eslint valid-jsdoc: ["error", { "requireReturn": false }] */

/**
 * @typedef {Object} config
 * @type {Object}
 * @property {Boolean} debug
 * @property {String} id
 * @property {Array} excludeRoutes
 */
const config = {
  debug: false,
  locale: 'es_MX',
  id: '',
  excludeRoutes: [],
  disable: false,
  router: null,
}

const CONSOLE_PREFIX = '[Vue Facebook Pixel]'

const log = (...args) => {
  if (config.debug) {
    const c = window.console
    let type = args[0]
    if (type in c) {
      args.shift()
    } else {
      type = 'log'
    }
    args.unshift(CONSOLE_PREFIX)
    // eslint-disable-next-line no-console
    c[type](...args)
  }
}

// Private functions

const fbqEnabled = () => {
  if (typeof window.fbq === 'undefined') {
    if (config.debug) {
      // eslint-disable-next-line no-console
      log('warn', '`window.fbq` is not defined, skipping')
    }
    return false
  }
  return true
}

/* eslint-disable prefer-const */
/* eslint-disable no-underscore-dangle */
function loadScript() {
  return new Promise((resolve, reject) => {
    const url = `https://connect.facebook.net/${config.locale}/fbevents.js`
    // eslint-disable-next-line no-multi-assign
    let local = (window.fbq = (...args) => {
      // eslint-disable-next-line no-unused-expressions
      local.callMethod ? local.callMethod(args) : local.queue.push(args)
    })
    if (!window._fbq) window._fbq = local
    local.push = local
    local.loaded = !0
    local.version = '2.0'
    local.queue = []
    let head = document.head || document.getElementsByTagName('head')[0]
    const script = document.createElement('script')
    script.async = true
    script.id = 'fbq'
    script.src = url
    script.charset = 'utf8'

    head.appendChild(script)

    script.onload = resolve
    script.onerror = reject
  })
}
/* eslint-enable no-underscore-dangle */
/* eslint-enable prefer-const */

/**
 * Submit a raw query to fbq for when the wrapper limits user on what they need.
 * This makes it still possible to access the plain Analytics api.
 *
 * @param {*[]} args - Argumentos para el query raw.
 * @returns {undefined} Nada.
 */
const query = (...args) => {
  if (!fbqEnabled()) return

  if (config.debug) {
    log('groupCollapsed', 'Raw query')
    log('With data:', args)
    log('groupEnd')
  }

  window.fbq(...args)
}

const toggleConsent = (enable, data = {}) => {
  config.disable = !enable
  // config.fbq('consent', enable ? 'grant' : 'revoke', data)
  query('consent', enable ? 'grant' : 'revoke', data)
}

// Public functions

/**
 * Init facebook tracking pixel.
 *
 * @param  {string} id - La id de trackeo de la app.
 * @param  {Object} [data={}] - Argumentos que pasar a fbq.
 */
const init = (id, data = {}) => {
  if (!fbqEnabled()) return

  if (config.debug) {
    log('log', `Initializing app ${id}`)
  }

  query('init', id, data)
}

/**
 * Event tracking.
 *
 * @param  {string} name - El nombre del evento.
 * @param  {Object} [data={}] - Los parametros a pasar.
 */
const event = (name, data = {}) => {
  if (!fbqEnabled()) return

  if (config.debug) {
    log('groupCollapsed', `Track event "${name}"`)
    log(`With data: ${data}`)
    log('groupEnd')
  }

  query('track', name, data)
}

const trackRoutes = () => {
  // Support for Vue-Router:
  if (config.router) {
    const { excludeRoutes } = config

    config.router.afterEach(({ name }) => {
      if (excludeRoutes.length && excludeRoutes.indexOf(name) !== -1) {
        return
      }
      config.$vue.nextTick().then(() => {
        const $analytics = config.$vue.$analytics || window.$nuxt.$analytics
        $analytics.fbq.event('PageView')
      })
    })
  }
}

const enable = () => {
  if (fbqEnabled()) return
  loadScript()
    .then(() => {
      toggleConsent(true)
      init(config.id)
      event('PageView')
      trackRoutes(config)
    })
    .catch(e => {
      // eslint-disable-next-line no-console
      console.error(`${CONSOLE_PREFIX} Error loading the script`, e)
    })
}
const disable = () => {
  toggleConsent(false)
  delete window.fbq
  // eslint-disable-next-line
  delete window._fbq
  const element = document.getElementById('fbq')
  if (element) {
    element.parentNode.removeChild(element)
  }
}

const VueFacebookPixel = {
  /**
   * Vue installer.
   *
   * @param {Vue} Vue - The vue instance.
   * @param {Object} [options={}] - The options for the plugin.
   */
  install(Vue, options) {
    Object.assign(config, options, { $vue: Vue })
    const { debug } = config

    if (!config.id) {
      throw new Error(
        // eslint-disable-next-line max-len
        `${CONSOLE_PREFIX} Missing the "id" parameter. Add at least one tracking domain ID`
      )
    }

    config.debug = !!debug

    if (!Vue.prototype.$analytics) {
      Vue.prototype.$analytics = {}
    }

    // Setting values for both Vue and component instances
    // Usage:
    // `this.$analytics.fbq.event('eventName', params)`

    Vue.prototype.$analytics.fbq = {
      event,
      query,
      enable,
      disable,
      init,
    }
    // Vue.prototype.$analytics = Vue.$analytics
    setTimeout(config.disable ? disable() : enable(), 1 * 1000)
  },
}

export default VueFacebookPixel
