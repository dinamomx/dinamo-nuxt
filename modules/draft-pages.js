import { readFileSync } from 'fs'

const matchScript = /<script>(.*)<\/script>/s
// Inexact matcher
const checkForDraft = /^\/\*\s+DRAFT\s+\*\/$/gm
/**
 * @param {import('@nuxt/types/config/router').NuxtRouteConfig[]} routes Las rutas a filtrar
 */
function redundantRemove(routes) {
  /** @type {number[]} */
  for (let i = 0; i < routes.length; i++) {
    const route = routes[i]
    // Primero borramos los hijos para evitar trabajar sobre huerfanis
    if (route.children) {
      redundantRemove(route.children)
    }
    let module = route.component
    // Nos aseguramos de que el componente no sea un modulo
    if (typeof module === 'string') {
      // Leemos el archivo como texto
      module = readFileSync(module, { encoding: 'utf8' })
      // Aislamos el tag de script para evitar errores
      const [, scriptTag] = matchScript.exec(module) || []
      // Buscamos (burdamente) si el componente tiene un
      const hasDraft = scriptTag.match(checkForDraft)
      if (hasDraft) {
        // eslint-disable-next-line no-console
        console.warn(`Route deleted becouse is a draft ${route.component}`)
        routes.splice(i, 1)
      }
    } else {
      /* eslint-disable-next-line no-console */
      console.error(
        `La ruta ${route.path} tiene el módulo cargado, esto es inesperado`
      )
    }
  }
}
/** @type {import('@nuxt/types').Module} */
const draftPagesModule = function draftPagesModule() {
  if (!this.options.router) {
    this.options.router = {}
  }
  const previousExtend = this.options.router.extendRoutes || (() => null)

  if (process.env.NODE_ENV === 'production' && !process.env.BUILD_DRAFT) {
    this.options.router.extendRoutes = (routes, resolve) => {
      previousExtend(routes, resolve)
      redundantRemove(routes)
    }
  }
}

export default draftPagesModule
