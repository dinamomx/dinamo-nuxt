/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable global-require */
const makeRitmo = require('./utils/ritmo')

/**
 * Pare encontrar tu scaleRatio: https://www.modularscale.com/
 * Para encontrar tu lineHeightFactor https://www.gridlover.net/try
 */
const { spacings, fontSizes, lineHeights } = makeRitmo({
  scaleRatio: 1.2,
  baseFontSize: 16,
  lineHeightFactor: 1.75,
  unit: 'rem',
})
// Si tienes dudas
// console.log({ spacings, fontSizes, lineHeights })

const colors = {
  text: '#403f3c',
  clear: '#e9ebec',
  brand: {
    naranja: '#F83F28',
    azul: '#211F35',
  },
}

const screens = {
  xsm: '360px',
  sm: '640px',
  md: '768px',
  lg: '1024px',
  xl: '1280px',
  xxl: '1440px',
}

const fonts = {
  sans: [
    'Intro',
    'system-ui',
    'BlinkMacSystemFont',
    '-apple-system',
    'Segoe UI',
    'Roboto',
    'Oxygen',
    'Ubuntu',
    'Cantarell',
    'Fira Sans',
    'Droid Sans',
    'Helvetica Neue',
    'sans-serif',
  ],
  serif: [
    'Constantia',
    'Lucida Bright',
    'Lucidabright',
    'Lucida Serif',
    'Lucida',
    'DejaVu Serif',
    'Bitstream Vera Serif',
    'Liberation Serif',
    'Georgia',
    'serif',
  ],
  mono: [
    'Menlo',
    'Monaco',
    'Consolas',
    'Liberation Mono',
    'Courier New',
    'monospace',
  ],
}

const percentages = {
  '1/2': '50%',
  '1/3': '33.33333%',
  '2/3': '66.66667%',
  '1/4': '25%',
  '2/4': '50%',
  '3/4': '75%',
  '1/5': '20%',
  '2/5': '40%',
  '3/5': '60%',
  '4/5': '80%',
  '1/6': '16.66667%',
  '2/6': '33.33333%',
  '3/6': '50%',
  '4/6': '66.66667%',
  '5/6': '83.33333%',
  '1/12': '8.33333%',
  '2/12': '16.66667%',
  '3/12': '25%',
  '4/12': '33.33333%',
  '5/12': '41.66667%',
  '6/12': '50%',
  '7/12': '58.33333%',
  '8/12': '66.66667%',
  '9/12': '75%',
  '10/12': '83.33333%',
  '11/12': '91.66667%',
}

const zIndex = {
  auto: 'auto',
  0: 0,
  1: 1,
  2: 2,
  3: 3,
  4: 4,
  5: 5,
  6: 6,
  7: 7,
  8: 8,
  9: 9,
  10: 10,
  20: 20,
  30: 30,
  40: 40,
  50: 50,
}

module.exports = {
  purge: false,
  /**
   * Las opciones para los estilos
   */
  theme: {
    screens,
    spacings,
    percentages,
    backgroundColor: (theme) => theme('colors'),
    borderColor: (theme) => ({
      ...theme('colors'),
      default: theme('colors.gray.300', 'currentColor'),
    }),
    fontFamily: fonts,
    fontSize: fontSizes,
    height: (theme) => ({
      auto: 'auto',
      ...theme('spacings'),
      ...theme('percentages'),
      full: '100%',
      screen: '100vh',
    }),
    lineHeight: lineHeights,
    margin: (theme, { negative }) => ({
      auto: 'auto',
      ...theme('spacings'),
      ...theme('percentages'),
      '-px': '-1px',
      ...negative(theme('spacings')),
      ...negative(theme('percentages')),
    }),
    padding: (theme) => ({
      px: '1px',
      0: '0',
      ...theme('spacings'),
      ...theme('percentages'),
    }),
    textColor: (theme) => theme('colors'),
    width: (theme) => ({
      auto: 'auto',
      ...theme('spacings'),
      ...theme('percentages'),
      full: '100%',
      screen: '100vw',
    }),
    container: {
      center: true,
      // padding: spacings[2], // maybe?
    },
    zIndex,
    aspectRatio: {
      square: [1, 1],
      '16/9': [16, 9],
      '4/3': [4, 3],
      '3/2': [3, 2],
      '21/9': [21, 9],
    },
    extend: {
      colors,
    },
  },
  /**
   * Añadiendo cualquiera de los elementos arriba con valor false
   * desahbilita esa clase, ej: `container: false` desahbilita la clase
   * `.container`
   */
  corePlugins: {},

  plugins: [
    /**
     * Ratio de aspecto
     * uso: `.aspect-ratio-square` o `.aspect-ratio-16/9`
     */
    require('tailwindcss-aspect-ratio'),
    require('@tailwindcss/custom-forms'),
  ],
}
