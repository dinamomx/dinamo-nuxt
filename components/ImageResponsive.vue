<docs>
  Componente para usar imagenes responsivas (srcset + sizes) con lazy load

  ```jsx
  <image-responsive
    src="placeholder/example.jpg"
    alt="Imagen de desmostración"
  />
  ```

  Imagen remota

  ```jsx
  <image-responsive
    src="/images/placeholder/example.jpg"
    baseUrl="https://plantilla.dinamo.mx"
    alt="Imagen de desmostración"
  />
  ```
</docs>

<template>
  <picture
    class="image-responsive"
    :class="[{ 'is-failed': failed }, { 'is-loaded': loaded }]"
    :style="[imageGradient]"
  >
    <source
      v-if="webp && srcsetWebp"
      :srcset="isVisible ? srcsetWebp : ''"
      :sizes="sizes"
      type="image/webp"
    />
    <slot
      :imageLoaded="imageLoaded"
      :loaded="loaded"
      :normalSrc="normalSrc"
      :srcset="srcset"
      :sizes="sizes"
      :imageClasses="imageClasses"
      :src="src"
      :isVisible="isVisible"
    >
      <img
        v-if="responsive"
        :src="isVisible ? normalSrc : ''"
        :srcset="isVisible ? srcset : ''"
        :sizes="sizes"
        class="image-responsive__img"
        :class="imageClasses"
        :loading="lazy ? 'lazy' : 'eager'"
        :style="{ opacity: loaded ? 1 : 0 }"
        :alt="alt"
        :height="height"
        :width="width"
        @load="imageLoaded"
        @error="imageLoaded"
      />
      <img
        v-else
        :src="isVisible ? src : ''"
        class="image-responsive__img"
        :class="imageClasses"
        :loading="lazy ? 'lazy' : 'eager'"
        :style="{ opacity: loaded ? 1 : 0 }"
        :height="height"
        :width="width"
        :alt="alt"
        @load="imageLoaded"
        @error="imageLoaded"
      />
    </slot>
  </picture>
</template>

<script>
/**
 * @typedef {object} ImageSizes
 * @property {string} separator El separador de la imagen `fondo-largo*_*xl.png`
 * @property {string} word La palabra que define el tamaño `fondo-largo_*xl*.png`
 * @property {string} size El ancho de la imagen `480w`
 */
/**
 * Quita el primer carácter si este es una diagonal.
 *
 * @param {string} string - La cadena a quitarle el /.
 * @returns {string} La cadena sin el /.
 */
function replaceFirstSlash(string = '') {
  return string.startsWith('/') ? string.slice(1) : string
}

const imagePathExtentionRegex = /(\S+)?\/(\S*)(\.jpg|\.png|\.svg)$/

/**
 * Formatea una string para srcset basada en un tamaño
 *
 * @param {import('types').ImageObject} image La imagen
 * @param {ImageSizes} size El tamaño
 * @returns {string}
 */
function formatSrcset(image, size) {
  return image.route
    ? `${image.route}/${image.name}${size.separator}${size.word}${image.format} ${size.size}`
    : `${image.name}${size.separator}${size.word}${image.format} ${size.size}`
}
/**
 * Imagen Responsiva con carga demorada (LazyLoad)
 *
 * @version 1.1
 * @author César Valadez | DinamoMX
 * @since 2018-01-19
 * @description Imagen Responsiva, esto es un wrapper para el simple img
 * @see https://www.smashingmagazine.com/2014/05/responsive-images-done-right-guide-picture-srcset/
 * @see https://ericportis.com/posts/2014/srcset-sizes/
 */
export default {
  name: 'ImageResponsive',
  props: {
    /**
     * La url o path de la imagen
     *
     * @example '/placeholder/demo.jpg'
     */
    src: {
      type: String,
      required: true,
    },
    /**
     * El texto alternativo de la imagen
     */
    alt: {
      type: String,
      default: 'Placeholder, please fill me',
    },
    /**
     * Los Tamaños de la imagen de acuerdo al atributo nativo sizes
     * el ancho esperado de la imagen de acuerdo al ancho
     * de la pantalla.
     *
     */
    sizes: {
      type: String,
      default: '(min-width: 769px) 90vw, 100vw',
    },
    /**
     * Los archivos de tamaño de imagen a buscar
     * corresponde al atributo nativo srcset
     * Guía a srcset y sizes https://ericportis.com/posts/2014/srcset-sizes/
     *
     */
    imageSizes: {
      /** @type {{ new (): ImageSizes[] }} */
      type: Array,
      default: () => [
        {
          separator: '_',
          word: 'xs',
          size: '480w',
        },
        {
          separator: '_',
          word: 'md',
          size: '720w',
        },
        {
          separator: '_',
          word: 'xl',
          size: '1480w',
        },
      ],
    },

    /**
     * Los colores del gradiente mientras carga la imagen
     */
    colors: {
      /** @type {{new (): string[]}}} */
      type: Array,
      default: () => ['#F83F28', '#472533', '#211F35'],
    },
    /**
     * Hacer o no responsiva la imagen
     */
    responsive: {
      type: Boolean,
      default: true,
    },
    /**
     * Url Base para las imagenes
     */
    baseUrl: {
      type: String,
      default: '/images',
    },
    /**
     * Usar también el formato webp
     */
    webp: {
      type: Boolean,
      default: true,
    },
    /**
     * Clases adicionales para el elemento img
     */
    imageClasses: {
      type: [Array, Object, String],
      default: '',
    },
    /**
     * Carga demorada
     */
    lazy: {
      type: Boolean,
      default: true,
    },

    width: {
      type: [String, Number],
      default: '',
    },

    height: {
      type: [String, Number],
      default: '',
    },
  },
  data() {
    return {
      /**
       * Carga exitosa de la imagen
       *
       * @type {boolean}
       */
      loaded: false,
      /**
       * Fallo en la carga de la imagen
       *
       * @type {boolean}
       */
      failed: false,
      /**
       * La imagen es visible
       *
       * @type {boolean}
       */
      isVisible: false,
      /**
       * La instancia de IntersectionObserver
       *
       * @type {IntersectionObserver|null}
       */
      observer: null,
      /** Este navegador soporta el attributo loading? */
      supportsLoadingAttr: false,
    }
  },
  computed: {
    /**
     * Separa la ruta, nombre del archivo y formato de la imagen.
     *
     * @requires this.src
     * @returns {import('types').ImageObject} Partes de la ruta de la imagen.
     */
    image() {
      let [, route = '', name, format] =
        this.src.match(imagePathExtentionRegex) || []
      name = replaceFirstSlash(name)
      return { route, name, format }
    },
    /**
     * Convierte el array de colores a una string css.
     *
     * @returns {{'--background'?: string}} CSS linear-gradient.
     */
    imageGradient() {
      if (this.failed) {
        return {}
      }
      const colors = this.colors.join(', ')

      return {
        '--background': `linear-gradient(35deg, ${colors})`,
      }
    },
    /**
     * La imagen de menor tamaño se usa en src.
     *
     * @returns {string} La url más pequeña de la imagen.
     */
    normalSrc() {
      if (!this.responsive) {
        return ''
      }
      const [size] = this.imageSizes
      const { image } = this
      return image.route
        ? `${image.route}/${image.name}${size.separator}${size.word}${image.format}`
        : `${image.name}${size.separator}${size.word}${image.format}`
    },
    /**
     * El valor para el atributo srcset.
     *
     * @returns {string} El srcset.
     * @see [Artículo MDN sobre las imagenes adaptables] https://developer.mozilla.org/es/docs/Learn/HTML/Multimedia_and_embedding/Responsive_images
     */
    srcset() {
      if (!this.responsive) {
        return ''
      }
      const arrayOfSizes = []
      for (let i = 0; i < this.imageSizes.length; i++) {
        const size = this.imageSizes[i]
        const result = formatSrcset(this.image, size)
        arrayOfSizes.push(result)
      }
      return arrayOfSizes.join(', ')
    },
    /** @returns {string} */
    srcsetWebp() {
      if (!this.webp) {
        return ''
      }
      const r = new RegExp(this.image.format, 'g')
      return this.responsive
        ? this.srcset.replace(r, '.webp')
        : this.src.replace(r, '.webp')
    },
  },
  watch: {
    src() {
      this.checkCapabilities()
    },
  },
  created() {
    if (process.client) {
      this.checkCapabilities()
    }
  },
  mounted() {
    // Alredy checked for support
    if (!this.supportsLoadingAttr && this.observer) {
      this.observer.observe(this.$el)
    }
  },
  methods: {
    checkCapabilities() {
      // Check for native support
      if ('loading' in HTMLImageElement.prototype) {
        // Browser supports `loading`..
        this.supportsLoadingAttr = true
        this.isVisible = true
      } else if ('IntersectionObserver' in window) {
        this.initObsever()
      } else {
        this.isVisible = true
        // eslint-disable-next-line no-console
        console.warn(
          'No soporta intersection observer ni el loading atribute en img'
        )
      }
    },
    initObsever() {
      if (this.observer) {
        this.observer.disconnect()
      }
      this.observer = new IntersectionObserver(
        (entries) => {
          const entry = entries[0]
          if (entry.isIntersecting && this.observer) {
            this.isVisible = true
            this.observer.unobserve(this.$el)
            this.observer = null
          }
        },
        {
          rootMargin: '20px 20px 20px 20px',
        }
      )
    },
    /**
     * Es llamado cuando la imagen se carga o falla.
     *
     * @param {Event} event - El evento de la carga.
     */
    imageLoaded(event) {
      if (event.type === 'error') {
        this.failed = this.isVisible
      } else {
        this.loaded = true
      }
    },
  },
}
</script>
