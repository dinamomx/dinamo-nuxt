/* eslint-disable @typescript-eslint/ban-ts-comment */
/* eslint-disable global-require */
/* eslint-disable import/no-extraneous-dependencies */
// @ts-check
import { website, localBussiness, organization } from './utils/schema'

require('dotenv').config()

const {
  PRODUCTION_HOST = 'www.plantilla.dinamo.mx',
  GA_ID = 'UA-143980-7',
  NODE_ENV,
} = process.env

const isProduction = NODE_ENV === 'production'

/**
 * Headers for the page
 *
 * @type {import('vue-meta').MetaInfo}
 */
const head = {
  meta: [
    {
      name: 'description',
      hid: 'description',
      content: 'Plantilla de dínamo con Nuxt',
    },
  ],
  link: [],
  titleTemplate: (titleChunk = '') =>
    titleChunk
      ? `${titleChunk} - Dinamo Agencia de Comunicación`
      : 'Dinamo Agencia de Comunicación',
  script: [
    {
      innerHTML: JSON.stringify(website),
      type: 'application/ld+json',
    },
    {
      innerHTML: JSON.stringify(localBussiness),
      type: 'application/ld+json',
    },
    {
      innerHTML: JSON.stringify(organization),
      type: 'application/ld+json',
    },
  ],
  __dangerouslyDisableSanitizers: ['script'],
}

/**
 * CSS que solo es pensado para desarrollo
 */
const developmentCss = [
  '~assets/styles/overlay-grid.css',
  // '~assets/styles/super-debug.scss',
]

/**
 * CSS para la página
 */
let css = [
  '@fortawesome/fontawesome-svg-core/styles.css',
  '~assets/styles/transitions.scss',
  '~assets/styles/base.scss', // Resets de estilos
  '~assets/styles/components.scss', // Componentes
  '~assets/styles/utilities.scss', // Componentes
]

/**
 * Configuración para postCSS
 *
 * @type {import('@nuxt/types/config/build').PostcssConfiguration}
 */
const postCSSConfiguration = {
  plugins: {
    'postcss-import': {},
    'postcss-url': {},
    tailwindcss: {},
    'postcss-color-function': {},
    // Solo habilitar si se tiene que soportar ie11
    // 'postcss-object-fit-images': {},
    // SEE: https://preset-env.cssdb.org/
    'postcss-preset-env': isProduction
      ? {
          cascade: false,
          grid: true,
          // SEE: https://github.com/postcss/autoprefixer#grid-autoplacement-support-in-ie
          autoprefixer: {
            grid: 'no-autoplace',
          },
        }
      : false,
    'postcss-reporter': {
      clearReportedMessages: true,
    },
    cssnano: isProduction
      ? {
          preset: 'default',
          discardComments: {
            removeAll: true,
          },
          zindex: 100,
        }
      : false,
  },
}
/**
 * Modules de nuxt para usarse solo en producción
 */
const productionModules = [
  /**
   * Limpiador de CSS
   */
  'nuxt-purgecss',
  /** Elimina las páginas marcadas como "DRAFT" */
  './modules/draft-pages',
  /** Cuando se busca un service worker */
  // '@nuxtjs/workbox',
]

/**
 * Modulos pensados a usarse solo en desarrrollo
 *
 * @type {string[]}
 */
const developmentModules = []

/**
 * Modulos que siempre se cargan
 */
let modules = ['@nuxtjs/svg-sprite', '@nuxtjs/style-resources']

/**
 * Plugins de webpack
 */
const webPackPlugins = isProduction
  ? []
  : [new (require('lodash-webpack-plugin'))()]

/**
 * Acomodamos la configuración de acuerdo a producción o desarrollo
 */
if (isProduction) {
  modules = modules.concat(productionModules)
} else {
  css = css.concat(developmentCss)
  modules = modules.concat(developmentModules)
}

/** @type {import('@nuxt/types').Configuration} */
const nuxtConfig = {
  mode: 'universal',
  head,
  /**
   * Variables de entorno para la página
   */
  env: {
    PRODUCTION_HOST,
    /** Configuración de google-analytics */
    GA_ID,
  },
  /**
   * Opciones de vue-router
   */
  router: {
    linkActiveClass: 'is-active',
    linkExactActiveClass: 'is-active--exact',
    middleware: [],
  },
  /**
   * Transición por defecto
   */
  pageTransition: 'page',
  /**
   * Crea un set para navegadores más vergras
   * https://nuxtjs.org/api/configuration-modern#the-modern-property
   */
  modern: isProduction,
  /**
   * CSS global
   */
  css,
  /*
   ** Customize the progress bar color
   */
  loading: {
    color: '#3B8070',
  },
  /**
   * Nuxt Plugins
   */
  plugins: [
    '~/plugins/font-awesome.js',
    '~/plugins/global-components.js',
    '~/plugins/vue-gtag.client.js',
    '~/plugins/vue-gtag.server.js',
    '~/plugins/detect-webp.client.js',
    '~/plugins/detect-webp.server.js',
    '~/plugins/tracking.client.js',
    '~/plugins/tracking.server.js',
    // '~/plugins/uaparser.server.js', // Activar solo si se usa vuex
    {
      src: '~plugins/scroll-track.js',
      ssr: false,
    },
  ],

  watch: ['./tailwind.config.js'],
  generate: {
    fallback: true,
    async routes() {
      // Importamos la función que genera las rutas
      const { generateRoutes } = require('./utils/route-generator')

      // Esa función genera arrays por lo que tenemos que mezclarlos para tener
      //   un array unidimencional
      return [
        // Acepta como argumentos la ruta donde se encuentran los archivos y la
        //   ruta que estos archivos van a renderizar
        ...(await generateRoutes('assets/content/checklists', 'checklists')),
        ...(await generateRoutes('assets/content/blog', 'blog')),
      ]
    },
  },
  /*
   ** Build configuration
   */
  build: {
    /**
     * La configuración de postcss es necesaria tenerla aquí más que en su
     * propio archivo por que puede ser quitar todas las características de nuxt
     * teniendo efectos adversos.
     */

    postcss: postCSSConfiguration,

    /**
     * Enable thread-loader in webpack building
     */
    parallel: !isProduction,
    /**
     * Enable cache of terser-webpack-plugin and cache-loader
     */
    cache: !isProduction,
    /**
     * Es necesario sobreescribir lo que hace babel por defecto
     */
    babel: {
      presets: ({ isServer }) => [
        [
          require.resolve('@nuxt/babel-preset-app'),
          {
            corejs: { version: 3 },
            buildTarget: isServer ? 'server' : 'client',
            // Incluir polyfills globales es mejor que no hacerlo
            useBuiltIns: 'entry',
            // Un poco menos de código a cambio de posibles errores
            loose: true,
            // Nuxt quiere usar ie 9, yo no.
            targets: isServer
              ? {
                  node: 10,
                }
              : {},
          },
        ],
      ],
      plugins: [
        // Reduce drásticamente el tamaño del bundle
        'lodash',
        '@babel/plugin-proposal-optional-chaining',
      ],
    },
    // Hace el css cacheable
    extractCSS: isProduction,
    // Alias el ícono de buefy a uno que soporta los íconos de font-awesome
    // @ts-ignore
    plugins: webPackPlugins,
    extend(config, { isDev, isClient }) {
      if (config.module) {
        // Añade loader para contenido en markdown con front-matter
        config.module.rules.push({
          test: /\.md$/,
          use: require.resolve('./utils/content-loader.js'),
        })
        // Añade un loader básico para yaml
        config.module.rules.push({
          test: /\.ya?ml$/,
          use: 'js-yaml-loader',
        })
        // Evita conflictos con el bloque de documentación
        config.module.rules.push({
          resourceQuery: /blockType=docs/,
          loader: require.resolve('./utils/documentation-block-loader.js'),
        })
        // Run ESLINT on save
        if (isDev && isClient) {
          config.module.rules.push({
            enforce: 'pre',
            test: /\.(jsx?|vue|tsx?)$/,
            loader: 'eslint-loader',
          })
        }
      }
      return config
    },
  },
  buildModules: ['@nuxt/typescript-build'],
  /**
   * Estos modulos son altamente recomendado en producción ya que facilitan
   * la incluisión de varias herramientas
   */
  modules,
  /**
   * Configuraciones para los modulos
   */
  styleResources: {
    scss: ['~/assets/styles/_variables.scss', '~/assets/styles/_mixins.scss'],
  },
  /**
   * Donde se encuentren alojados los íconos
   */
  svgSprite: {
    input: '~/assets/icons/svg/',
  },
  /**
   * Configuración del pixel de facebook
   */
  'facebook-pixel': {
    id: '661561957280518',
    debug: !isProduction,
    disable: true,
  },
  /**
   * Configuración para PurgeCSS
   */
  purgeCSS: {
    mode: 'webpack',
    whitelist: ['svg-inline--fa', 'is-active'],
    whitelistPatterns: [
      /[\w|-]+-(enter|leave|move)-?(active|to)?/,
      /^fa-/,
      /-fa$/,
      /^btn-burger/,
    ],
    whitelistPatternsChildren: [],
  },
}

export default nuxtConfig
