# Plantilla de NUXT para dinamo.

[![Netlify Status](https://api.netlify.com/api/v1/badges/45ec1094-691e-4096-9ea4-28b2a212d8a2/deploy-status)](https://app.netlify.com/sites/dinamo-nuxt/deploys)

[TOC]

## Objetivos

El objetivo de esta plantilla es definir un flujo y estructura de trabajo para
la mayoría de los sitios web que dínamo ejecuta para sus clientes.

Casos de uso para esta plantilla:

- Cualquier sitio web que consuma datos de una API con actualizaciones
  frecuentes y efectos en tiempo real
- Cualquier sitio web que requiera de una interacción compleja del usuario con
  la página (sesiones, consumo de datos)
- Cualquier sitio web con interacciones o navegación visual de alto calibre como
  [https:://yagodemarta.com](https:://yagodemarta.com)
- Cualquier sitio web que tenga como requerimiento renderizado o lógica del lado
  del servidor. Para este caso también considerar el uso de laravel
- Un panel de administración de una api (también considerar @vue/cli)

Casos no ideales para esta plantilla:

- Landing pages
- Sitios estáticos de baja interacción
- Sitios web que tengan lógica del lado del servidor y con poca interacción ya
  que el costo de hacer una API es muy alto.

## Stack

- [Nuxt.js](https://nuxtjs.org/) La base y todo lo que esto implica
  - [Vue.js](https://vuejs.org/v2/guide/)
  - [Vue Meta](https://vue-meta.nuxtjs.org/)
  - [Vue Router](https://router.vuejs.org/)
  - [Vuex](https://vuex.vuejs.org/)
- [TailwindCSS](https://tailwindcss.com) Framework de utilidad de css para
  desarrollo rápido de sitios web
  - [Custom Forms](https://github.com/tailwindcss/custom-forms)
  - [Aspect Ratio](https://github.com/webdna/tailwindcss-aspect-ratio)
- [PurgeCSS](https://www.purgecss.com) Limpieza de CSS no utilizado en la página
- [modularscale.js](https://www.modularscale.com) Para generación de tamaños de
  fuente
- [MardownIt](https://github.com/markdown-it/markdown-it) Para poder importar
  archivos markdown
  - [markdown-it-attrs](https://github.com/arve0/markdown-it-attrs) Plugin para
    poder darle atributos a los elementos en markdown
  - [markdown-it-checkbox](https://github.com/mcecot/markdown-it-checkbox)
    Plugin para poder usar checkboxes en listas
  - [markdown-it-container](https://github.com/markdown-it/markdown-it-container#readme)
    Plugin para crear contenedores personalizados con sintaxis de pandoc
- [Fontawesome Pro](https://fontawesome.com) Paquete de iconos
- [Nuxt style-resources](https://github.com/nuxt-community/style-resources-module)
  Nos ayuda a modularizar
- [Nuxt svg-sprite](https://github.com/nuxt-community/svg-sprite-module) Para
  generar sprites desde archivos svg
- [Nuxt purgecss](https://github.com/Developmint/nuxt-purgecss) Para limpiar css
  sin utilizar
- [Vue-Gtag](https://github.com/MatteoGabriele/vue-gtag) Para el uso con
  analíticas de google
- [netlify-cms](https://www.netlifycms.org/) Administración de contenido al
  estilo JAM

## Requerimientos

Actualmente solo damos soporte de ejecución a sistemas _UNIX like_ como macOs y
GNU/Linux

Para poder ejecutar la plantilla en todo su potencial debes tener instaladas las
siguientes herramientas

- NodeJS v12 o mayor (Se recomienda instalar con
  [fnm](https://github.com/Schniz/fnm))
- pnpm v4.x (Paquete global de nodejs)
- pm2 (Paquete global de nodejs)
- cwebp (Se suele instalar con el gestor de paquetes del sistema: brew, dnf,
  apt...)

## Después de clonar

Después de clonar, actualizar o forkear esta plantilla necesitarás ejecutar los
siguientes comandos para empezar a trabajar

```bash
# Instala lo que este configurado en el repositorio y branch seleccionado.
pnpm install # o pnpm i

# Inicia el proyecto en modo de desarrollo en el puerto 3000 o uno aleatorio si ese está ocupado
pnpm run dev
```

## Estructura de archivos

La mayor parte de la estructura es heradada de Nuxt.js.

```bash
.
├── assets # Aquí van colocados los diferentes tipos de assets de la págfina, los que pasan por webpack
│   ├── data # Contenido que corresponde a aspectos generales del sitio en formato json o yaml y se puede utilizar netlify-cms para modificarlo
│   │   ├── settings.json # El objetivo de este archivo es centralizar configuraciones únicas y globales del sitio, como el título base o la url de producción
│   │   ├── authors.yml # Ejemplo de archivo yaml modificable desde netlify-cms
│   │   └── cookie-definitions.json # Aquí se almacena la información de las cookies usadas en el sitio para darlas a conocer a los usuarios
│   ├── content # El contenido dínamico del sitio deberá ir aquí, también el contenido controlado por netlify-cms
│   │   ├── checklists # Aquí va el contenido de los checklist que nosotros llevamos para controlar el lanzamiento de una página.
│   │   ├── blog # En esta carpeta se encuentra el contenido de ejemplo para el blog.
│   │   └── faqs # En esta carpeta se encuentra el contenido de ejemplo para contenido arbitrario en el sitio como una sección de FAQ.
│   ├── icons # Todos los íconos tipo de imagen van en esta carpeta
│   │   └── svg # En particular los íconos svg van en esta carpeta para usarse con el modulo svg-sprite
│   ├── images # Las ímagenes del sitio que no son importadas dinámicamente
│   ├── sprite # En esta carpeta van los sprites generados por el módulo svg-sprite
│   └── styles # Todos los estilos del sitio y sus componentes van en esta carpeta
│       ├── _mixins.scss # Esta hoja se importa automáticamente y van todas las funciones y los mixins de sass
│       ├── _variables.scss # Esta hoja se importa automáticamente y contiene las variables de sass.
│       ├── base.scss # En esta hoja van los estilos escenciales para la visualización de la página, como resets de css
│       ├── _html.scss # Esta hoja se importa en base.scss y complementa el reset de css de tailwind
│       ├── _layout.scss # Esta hoja se importa en base.scss y contiene estilos de estructura básica general, osea los que se usan en más de una ruta
│       ├── _typography.scss # Esta hoja se importa en base.scss y tiene los estilos de control tipográfico como títlos y parrafos
│       ├── components # Aquí van solo los estilos de los componentes
│       ├── components.scss # En este archivo se importan todos los componentes que se usan globalmente
│       ├── overlay-grid.css # Esta hoja solo se importa en modo desarrollo y pinta una rejilla con columnas y filas sobre el documento
│       ├── transitions.scss # En esta hoja se almacenan las transiciones, keyframes y animaciones de vue para ser reutilizadas cuando sean necesarias
│       ├── _super-debug.scss # Esta hoja tiene un truco para resaltar todos los elementos de forma anidada y solo debe importarse cuando se desee usar.
│       └── utilities.scss # Esta es laúltima hoja en importarse y contiene todas las utilidades de tailwind
├── components # En esta carpeta se almacenan todos los componentes de vue.
│   ├── AlertCookie.vue # Este componente se debe importar solo a nivel layout por que controla al inicio el manejo de rastreo de acuerdo a las preferencias del usuario
│   ├── AlertDevelopment.vue # Este componente es muy simple y solo inserta una alerta de que dicho sitio no está configurado como producción
│   ├── BaseCard.vue # La tarjeta básica inspirada en bulma
│   ├── FormGeneric.vue # El formulario base para la mayoría de las situaciones
│   ├── HeroGeneric.vue # Un Hero genérico que se reutiliza en muchos sitios
│   ├── ImageResponsive.vue # Este componente facilita el uso de imagenes responsivas con lazyloading retrocompatible
│   ├── LinkWhatsapp.vue # Un componente genérico que renderiza un enlace de whatsapp con un texto predeterminado para el mensaje
│   ├── TheFooter.vue # El pie de la página
│   ├── TheHeaderBurger.vue # El botón de hamburguesa para el header
│   └── TheHeader.vue # El header de la pagina
├── content # En caso de tener mucho contenido con estructura similar (ej: un blog) aquí se almacenan los archivos md o yaml para importarse posteriormente
├── layouts # Aquí van los diferentes layouts de la página, recuerda se pueden hacer transiciones entre estos
│   ├── default.vue # El layout por defecto
│   ├── empty.vue # Un layout sin header,footer,cookies o alertas
│   └── error.vue # Este archivo es engañoso ya que funciona como página de error genérica y no como layout.
├── middleware # Aquį va todo el middleware que se ejecutará en la página
│   └── meta.js # Este middleware está desactivado por defecto, usa vuex para registrar los cambios con la propiedad `meta` de cada página
├── mixins # Almacena mixins de vue
│   ├── external-link.js # Detecta enalces externos o internos, útili para cambiar entre <n-link> y <a>
│   ├── form-mixin.js # El mixin de formualrios con google-sheets
├── modules # Módulos de nuxt hechos en casa
│   └── facebook-pixel # Módulo para el pixel de facebook
├── pages # Aquí van las páginas del sitio.
├── plugins # Los plugins o arhivos que se importan y se ejecutan inmediatamente con la carga de la página
│   ├── font-awesome.js # Aquí se importan los íconos de font-awesome
│   ├── global-components.js # Aquí se registran los componentes globales para usarse en el sitio
│   ├── vue-gtag.{client,server}.js # Aquí se aplica la instalación de vue-gtag
│   └── scroll-track.js # Rastrea el comportamiento de scroll y envia analíticas
├── scripts # Scripts para ejecutarse con la shell
├── private # Si este sitio tiene un certificado o claves que se puedan guardar en el repo aquí van
├── static # Los archivos que se copian directamente a la versión publicada del proyecto sin pasar por webpack
│   ├── humans.txt # Los créditos del equipo
│   ├── icon.png # Este archivo png de 500x500 puede generar el favico si se usa el módulo @nuxtjs/pwa-icon
│   └── robots.txt # El robots, por defecto bloquea tráfico de buscadores
├── utils # Scripts de ayuda o miscelaneos
│   ├── content-loader.js # Loader para webpack para cargar archivos markdown
│   ├── docs-loader.js # Para permitir el uso del bloque <doc> en los archivos vue
│   ├── index.js # Utilidades genéricas de js
│   ├── markdown-containers.js # Containers para usarse con markdown-it y markdown-it-containers
│   ├── mutations.js # Las mutaciones para vuex
│   ├── nginx.conf # La configuración de nginx para el sitio
│   ├── ritmo.js # Genera valores para uso con ritmo vertical
│   ├── schema.js # Genera datos de Schema.org
│   └── store-example.js # Plantilla de vuex
├── types # Los tipos de typescript para usarse en el proyecto.
├── app.html # La plantilla base sobre la cual se renderiza toda la app
├── ecosystem.config.js # Este archivo tiene las instrucciones para un deploy con Node.js como backend y pm2 como manager.
├── store # Aquí va el store de vuex y sus módulos
├── netlify.toml # Configuración de netlify para su configuración automática.
├── nuxt.config.js # La configuración de nuxt, la más importante y aquí esta casi todo lo que tenga que ver con runtime
├── subircambios # Archivo ejecutable para subir el proyecto con rsync
├── tailwind.config.js # La configuración de tailwind, colores, tamaños etc... MANTENER EN SINCRONÍA CON LAS VARIABLES DE SASS
├── uploadbuild.sh # Script para subir los cambios usando pm2.
├── uploadignore.txt # Lista negra de archivos a no subir
├── webpack.config.js # Solo un placeholder para la configuración de eslint
├── .editorconfig # Configura los espacios para editores que no soportan prettier o eslint
├── .eslintignore # Lista negra para eslint
├── .eslintrc.js # La configuración de eslint para mantener la calidad y estilo del código
├── .npmrc # Configuración de npm, pnpm y node_modules
├── .prettierignore # Lista negra para prettier
├── .prettierrc # La configuración de prettier
└── .stylelintrc # La configuración de stylelint para mantener la calidad y estilo del código

```

## Deploy

Hay una serie de scripts preparados para "deploy" el proyecto. Dependiendo de la
forma que se desee usar, está el uso con node js y html estático.

Hay que configurar los archivos correspondientes `ecosystem.conf.js`,
`uploadbuild.sh` o `subircambios`

```bash
# Para subir los archivos estáticos
pnpm run deploy:static
# Para por primera vez subir el proyecto con nodejs
pnpm run deploy:production:setup
# Para actualizar el proyecto con nodejs
pnpm run deploy:production
```

### Usando netlify

Netlify nos permite hacer deploy con un solo push a master, pero requiere
configuración inicial. Esta se puede realizar a través de la interfaz
[web de netelify](https://app.netlify.com) o desde
[la terminal](https://docs.netlify.com/cli/get-started/#installation).

> Es muy importante tener en cuenta que **sí se usa netlify para producción** se
> debe cambiar la configuración de netlify para eliminar las cabeceras que
> previenen el indexado en buscadores.

## Contenido dínamico con archivos md o json

En caso de usar contenido dínamico con json o markdown que sea responsable de
renderizar rutas, como el blog o los checklist. Se debe configurar el generado
de las rutas en la configuración de nuxt `generate.routes` siguiendo el ejemplo
presente.

## Linter

Estamos usando un verificiador de estilo y errores automático para ayudarnos
tanto a mantener un código consistente como fácil de mantener y con la menor
cantidad de errores posible.

El estilo de código que usamos y por consiguiente las reglas que se siguen son
una mezcla de
[airbnb/base](https://github.com/airbnb/javascript#table-of-contents) y
[vue/recomended](https://vuejs.org/v2/style-guide/). Airbnb funciona como base y
vue como complemento. En los enlaces están las referencias a las reglas que se
usan con ejemplo de uso y razonamiento detrás de la regla. En el caso de las
reglas propietarias de vue
[esta es la referencia completa](https://github.com/vuejs/eslint-plugin-vue#bulb-rules)
ya que sigue en desarrollo.

Es muy recomendado instalar los complementos de eslint para el editor ya que
estos te explican en que consiste un error y así corregirlo conforme se escribe.

## VScode

La configuración del editor para el flujo común en dínamo es la siguiente:

```jsonc
{
  // Reglas para permitir a eslint funcionar como queremos
  "eslint.alwaysShowStatus": true,
  "eslint.codeAction.disableRuleComment": {
    "enable": true,
    "location": "separateLine"
  },
  "eslint.validate": [
    "javascript",
    "javascriptreact",
    "vue",
    "typescript",
    "typescriptreact"
  ],
  "editor.codeActionsOnSave": {
    "source.fixAll.eslint": true
  },
  "editor.formatOnPaste": true,
  "editor.formatOnSave": true,
  "editor.formatOnType": false,
  "javascript.format.enable": false,
  // Reglas para permitir a stylelint hacer su trabajo
  "css.validate": false,
  "scss.validate": false,
  "vetur.validation.style": false,
  "files.associations": {
    ".stylelintrc": "json"
  },
  // Reglas recomendadas pero pueden ser molestas para algunos
  "vetur.validation.template": true,
  "vetur.experimental.templateInterpolationService": true,
  // Reglas para PHP
  "emmet.includeLanguages": {
    "blade": "html"
  },
  // Cambia con tu ruta de usuario
  "phpcs.executablePath": "/home/cesar/.config/composer/vendor/bin/phpcs",
  // Reglas Genericas
  "git.autofetch": true,
  "git.confirmSync": false,
  "git.enableSmartCommit": true,
  "todo-tree.highlights.enabled": false,
  "editor.renderWhitespace": "boundary",
  "editor.rulers": [80, 120],
  "editor.cursorSmoothCaretAnimation": true,
  "editor.fontLigatures": true,
  "editor.renderControlCharacters": true,
  "projectManager.openInNewWindowWhenClickingInStatusBar": true,
  "terminal.integrated.cursorBlinking": true,
  "terminal.integrated.enableBell": true
}
```

Y requiere varios plugins que puedes instalar con el siguiente comando.

```bash
code --install-extension aaron-bond.better-comments
code --install-extension adpyke.vscode-sql-formatter
code --install-extension alefragnani.project-manager
code --install-extension bmewburn.vscode-intelephense-client
code --install-extension bradlc.vscode-tailwindcss
code --install-extension bungcip.better-toml
code --install-extension christian-kohler.npm-intellisense
code --install-extension cjhowe7.laravel-blade
code --install-extension dbaeumer.vscode-eslint
code --install-extension drKnoxy.eslint-disable-snippets
code --install-extension eamodio.gitlens
code --install-extension EditorConfig.EditorConfig
code --install-extension esbenp.prettier-vscode
code --install-extension fabiospampinato.vscode-todo-plus
code --install-extension Gruntfuggly.todo-tree
code --install-extension ikappas.phpcs
code --install-extension marclipovsky.string-manipulation
code --install-extension mikestead.dotenv
code --install-extension neilbrayfield.php-docblocker
code --install-extension onecentlin.laravel-blade
code --install-extension sdras.vue-vscode-extensionpack
code --install-extension thibaudcolas.stylelint
code --install-extension vscode-icons-team.vscode-icons
code --install-extension william-voyek.vscode-nginx
code --install-extension wmaurer.vscode-jumpy
```

## Meta y SEO

Los datos de SEO son manejados en diferentes partes y se describen en orden de
jerarquía

- `app.html`  
  Los datos que jamás cambian como el favico o la verificación de google, o el
  fb:app_id van en este archivo como html.
- `nuxt.config.js`  
  Los datos que se tienen que declarar de forma inicial pero cambian de ruta en
  ruta como el título o la descripción van aquí dentro del objeto `head` y
  requieren la propiedad `hid` que refleja ya sea `name` o `property` para que
  se puedan reemplazar dentro de la configuración de cada página.
- `utils/schema.js`  
  Este archivo es importado en `nuxt.config.js` y define la microdata o
  ["schema"](https://schema.org/)
- **Dentro de cada página**  
  En cada página debe haber un objeto `head` o una función que retorne un objeto
  con los datos que irían en el `<head>`. Se puede utilizar la función
  `commonHeadTags` para disminuir la repetición de código en ciertas situaciones

## Características añadidas por encima de nuxt

- Detección de webp de acuerdo al navegador con el plugin `detect-webp`
- Importación incluida de archivos markdown
- StyleResources: No tienes que importar variables de scss en tus archivos
- SVGSprites: Creación de sprites de svg usando el componente
  `<svg-icon name="sample" />` y la carpeta `~/assets/sprite/svg`
- Limpieza de css con purgeCSS
- Capacidad de omitir ciertas páginas de vue usando el comentario `/* DRAFT */`
  dentro del bloque `<script>`
