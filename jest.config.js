/** @type {jest.ProjectConfig & jest.GlobalConfig} */
const jestConfig = {
  setupFilesAfterEnv: ['./tests/unit/jest.setup.ts'],
  globals: {
    'ts-jest': {
      packageJson: './package.json',
      // tsconfig: './tests/unit/tsconfig.json',
      babelConfig: '.babelrc.js',
    },
    'vue-jest': {
      transform: {
        doc: './utils/documentation-block-loader',
      },
    },
  },
  moduleNameMapper: {
    '^@/(.*)$': '<rootDir>/$1',
    '^~/(.*)$': '<rootDir>/$1',
    '^vue$': 'vue/dist/vue.common.js',
  },

  moduleFileExtensions: ['ts', 'js', 'tsx', 'jsx', 'vue', 'json'],
  transform: {
    '^.+\\.tsx?$': 'ts-jest',
    '^.+\\.jsx?$': 'babel-jest',
    '.*\\.vue$': require.resolve('vue-jest'),
    '.+\\.(css|styl|less|sass|scss|png|jpg|ttf|woff|woff2|md)$':
      'jest-transform-stub',
  },
  snapshotSerializers: ['jest-serializer-vue'],
  coverageReporters: ['text', 'cobertura', 'text-summary'],
  cache: false,
  collectCoverage: true,
  reporters: ['default'],
  collectCoverageFrom: [
    '<rootDir>/components/**/*.vue',
    '<rootDir>/pages/**/*.vue',
    '<rootDir>/plugins/**/*.{js,ts}',
    '<rootDir>/utils/**/*.{js,ts}',
  ],
  testURL: 'http://mockeddomain.com',
  testMatch: ['**/tests/unit/**/*.spec.[jt]s?(x)', '**/__tests__/*.[jt]s?(x)'],
}

module.exports = jestConfig
