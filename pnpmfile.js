/* eslint-disable no-param-reassign */
module.exports = {
  hooks: {
    readPackage,
  },
}

const thisPackage = require('./package.json')

function readPackage(readedPackage, context) {
  // Override the manifest of foo@1 after downloading it from the registry
  // Replace all dependencies with bar@2
  if (
    readedPackage.name === 'vue-jest' &&
    (readedPackage.version.startsWith('4.') ||
      readedPackage.version.startsWith('3.'))
  ) {
    const originalTsJestVersion = readedPackage.dependencies['ts-jest']
    const thisTsJestVersion = thisPackage.devDependencies['ts-jest']
    context.log(originalTsJestVersion)
    readedPackage.dependencies['ts-jest'] = thisTsJestVersion
    // readedPackage.peerDependencies.jest = thisPackage.devDependencies.jest
    context.log(
      `vue-jest: ts-jest@${originalTsJestVersion} -> ${thisTsJestVersion}`
    )
    // context.log(
    //   `vue-jest: jest@${originalVersions.jest} -> ${thisPackage.peerDependencies.jest}`
    // )
  }

  return readedPackage
}
