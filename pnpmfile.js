/* eslint-disable max-len */
const packageJson = require('./package.json')

const vueVersion = packageJson.dependencies.vue

const vueOverrides = ['vue-server-renderer', 'vue-template-compiler']
const vueLoader = 'vue-loader'
const loaderVersion = packageJson.devDependencies[vueLoader]
function readPackage(pkg, ctx) {
  if (pkg.name === '@nuxt/vue-renderer' && pkg.version.startsWith('2.3.')) {
    vueOverrides.forEach(over => {
      const dep = pkg.dependencies[over]
      if (dep) {
        ctx.log(`${over}@${dep} => ${over}@${vueVersion} in @nuxt/vue-renderer`)
        pkg.dependencies[over] = vueVersion
      }
    })
  }
  if (pkg.name === '@nuxt/webpack' && pkg.version.startsWith('2.3.')) {
    const oldVersion = pkg.dependencies[vueLoader]
    pkg.dependencies[vueLoader] = loaderVersion
    ctx.log(
      `${vueLoader}@${oldVersion} => ${vueLoader}@${loaderVersion} in @nuxt/webpack`
    )
  }

  return pkg
}

module.exports = {
  hooks: {
    readPackage,
  },
}
